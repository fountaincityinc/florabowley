<?php
/**
 * The template for displaying the Shopping Cart.
 *
 * @package florabowley
 * @since florabowley 1.0
 */

/* Template Name: Shopping Cart */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$config = Presscore_Config::get_instance();
$config->set( 'template', 'fb-shopping-cart' );
presscore_config_base_init();

get_header(); ?>

	<?php if ( presscore_is_content_visible() ): ?>

		<div id="content" class="content" role="main">

			<?php SA_Shopping_Cart::shopping_cart(); ?>

		</div><!-- #content -->

		<?php do_action( 'presscore_after_content' ); ?>

	<?php endif; // if content visible ?>

<?php get_footer(); ?>
