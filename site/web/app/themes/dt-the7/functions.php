<?php
/**
 * Vogue theme.
 *
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

/**
 * Initialize theme.
 *
 * @since 1.0.0
 */
require( trailingslashit( get_template_directory() ) . 'inc/init.php' );

/**
 * Initialize 7apps stuff.
 *
 * @since 1.0.0
 */
require( trailingslashit( get_template_directory() ) . 'inc/7apps/init.php' );

/**
 *  Code to enable uploading SVG files
 *
 *
 */
function add_file_types_to_uploads($file_types) {
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );
  return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');


// TODO: Remove this - it's just for testing - DO NOT DEPLOY
// add_action('woocommerce_after_register_post_type', function () {
// 	if ( !is_admin() ) {
// 		$order = wc_get_order( 108661 );
// 		// $order = wc_get_order( 108651 );
// 		// print '<pre>'; var_dump( $order ); die();
// 		$emails = WC_Emails::instance();
// 		$emails->customer_invoice( $order );
// 	}
// });
