<?php
/**
 * Carbon Time Field
 */
class Carbon_Field_Time extends Carbon_Field {
	/**
	 * Timepicker type
	 * @var string
	 */
	protected $timepicker_type = 'time';

	/**
	 * Time format
	 */
	public $time_format = 'hh:mm tt';

	/**
	 * Interval Steps
	 */
	public $interval_step = array();

	/**
	 * Restraints
	 */
	public $restraints = array();

	/**
	 * Init method
	 */
	function init() {
		# Enqueue JS dependencies


		parent::init();
	}

	/**
	 * Renders the Field
	 */
	function render() {
		# Field Attributes
		$field_atts = array(
			'id'    => $this->get_id(),
			'type'  => 'text',
			'name'  => $this->get_name(),
			'value' => esc_attr($this->value),
			'class' => 'regular-text carbon-timepicker',
			'data-timepicker-type' => $this->timepicker_type,
			'data-time-format' => $this->get_time_format(),
		);

		# Add interval steps
		if ( $this->get_interval_step() ) {
			$field_atts['data-interval-step'] = $this->get_interval_step();
		}

		# Add restraints
		if ( $this->get_restraints() ) {
			$field_atts['data-restraints'] = $this->get_restraints();
		}

		# Add Required
		if ( $this->required ) {
			$field_atts['data-carbon-required'] = 'true';
		}

		# Implode Atts
		$output_atts = array();
		foreach ($field_atts as $att_name => $att_value) {
			$output_atts[] = $att_name . '="' . $att_value . '"';
		}
		$output_atts = implode(' ', $output_atts);

		# Output Field
		echo '<input ' . $output_atts . ' />';
	}

	/**
	 * Sets the time format
	 */
	public function set_time_format($time_format) {
		$this->time_format = $time_format;
		return $this;
	}

	/**
	 * Returns the time format
	 */
	public function get_time_format() {
		return $this->time_format;
	}

	/**
	 * Sets the interval step
	 */
	public function set_interval_step($interval_steps) {
		$output = array();

		foreach ($interval_steps as $step_name => $step_value) {
			$name = 'step' . ucwords($step_name);
			$output[] = $name . ':' . $step_value;
		}

		$this->interval_step = implode(',', $output);
		return $this;
	}

	/**
	 * Returns the interval step
	 */
	public function get_interval_step() {
		return $this->interval_step;
	}

	/**
	 * Sets the restraints
	 */
	public function set_restraints($restraints) {
		$output = array();

		foreach ($restraints as $restraint_name => $restraint_value) {
			$output[] = $restraint_name . ':' . $restraint_value;
		}

		$this->restraints = implode(',', $output);
		return $this;
	}

	/**
	 * Returns the restraints
	 */
	public function get_restraints() {
		return $this->restraints;
	}
}

/**
 * Carbon Datetime Field 
 */
class Carbon_Field_Datetime extends Carbon_Field_Time {
	protected $timepicker_type = "datetime";
}