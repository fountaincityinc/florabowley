## Carbon Timepicker

Extends Carbon Fields by adding a timepicker field.

## Usage


	<?php
	Carbon_Container::factory('theme_options', __('Theme Options', 'crb'))
		->add_fields(array(
			Carbon_Field::factory('timepicker', 'time', 'Time'),
		));
	?>

## Methods

`set_time_format` **(string) [optional]**

Sets the time format in which to display and save the Timepicker values.

Here is a list of allowed Format Characters:

<table>
	<thead>
		<tr>
			<th align="center">Character</th>
			<th>Definition</th>
			<th align="right">Examples</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td align="center"><strong>Time</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">t</td>
			<td>Lowercase</td>
			<td align="right">a, p</td>
		</tr>
		<tr>
			<td align="center">T</td>
			<td>Lowercase</td>
			<td align="right">A, P</td>
		</tr>
		<tr>
			<td align="center">tt</td>
			<td>Lowercase</td>
			<td align="right">am, pm</td>
		</tr>
		<tr>
			<td align="center">TT</td>
			<td>Uppercase</td>
			<td align="right">AM, PM</td>
		</tr>
		<tr>
			<td align="center">h</td>
			<td>Hour, 12-hour, without leading zeros</td>
			<td align="right">1–12</td>
		</tr>
		<tr>
			<td align="center">hh</td>
			<td>Hour, 12-hour, with leading zeros</td>
			<td align="right">01–12</td>
		</tr>
		<tr>
			<td align="center">H</td>
			<td>Hour, 24-hour, without leading zeros</td>
			<td align="right">0-23</td>
		</tr>
		<tr>
			<td align="center">HH</td>
			<td>Hour, 24-hour, with leading zeros</td>
			<td align="right">00–23</td>
		</tr>
		<tr>
			<td align="center">m</td>
			<td>Minutes, without leading zeros</td>
			<td align="right">0-59</td>
		</tr>
		<tr>
			<td align="center">mm</td>
			<td>Minutes, with leading zeros</td>
			<td align="right">00-59</td>
		</tr>
		<tr>
			<td align="center">s</td>
			<td>Seconds, without leading zeros</td>
			<td align="right">0-59</td>
		</tr>
		<tr>
			<td align="center">ss</td>
			<td>Seconds, with leading zeros</td>
			<td align="right">00-59</td>
		</tr>
	</tbody>
</table>

**The default date format is mm/dd/yyyy (e.g. 07/09/2014) and for now it can't be changed.**

	<?php
	Carbon_Container::factory('theme_options', __('Theme Options', 'crb'))
		->add_fields(array(
			Carbon_Field::factory('timepicker', 'time', 'Time')
				->set_time_format('hh:mm:ss tt'),
		));
	?>

----
`set_timepicker_type` **(string) [optional]**

Sets the type of the Timepicker. Allowed values are: **time [default]** and **datetime**

	<?php
	Carbon_Container::factory('theme_options', __('Theme Options', 'crb'))
		->add_fields(array(
			Carbon_Field::factory('timepicker', 'time', 'Time')
				->set_timepicker_type('datetime'),
		));
	?>

----
`set_interval_step` **(array) [optional]**

Sets the interval step for *hours*, *minutes* and *seconds*.

	<?php
	Carbon_Container::factory('theme_options', __('Theme Options', 'crb'))
		->add_fields(array(
			Carbon_Field::factory('timepicker', 'time', 'Time')
				->set_interval_step(array(
					'hour'   => '2',
					'minute' => '10',
					'second' => '10',
				)),
		));
	?>

----
`set_restraints` **(array) [optional]**

Sets restraints for *Hours*, *Minutes*, *Seconds* and *Dates*

<table>
	<thead>
		<tr>
			<th align="center">Type</th>
			<th>Definition</th>
			<th align="right">Default</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td align="center"><strong>Hours</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">hourMin</td>
			<td>The minimum hour allowed for all dates</td>
			<td align="right">0</td>
		</tr>
		<tr>
			<td align="center">hourMax</td>
			<td>The maximum hour allowed for all dates</td>
			<td align="right">23</td>
		</tr>
		<tr>
			<td align="center"><strong>Minutes</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">minuteMin</td>
			<td>The minimum minute allowed for all dates</td>
			<td align="right">0</td>
		</tr>
		<tr>
			<td align="center">minuteMax</td>
			<td>The maximum minute allowed for all dates</td>
			<td align="right">59</td>
		</tr>
		<tr>
			<td align="center"><strong>Seconds</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">secondMin</td>
			<td>The minimum second allowed for all dates</td>
			<td align="right">0</td>
		</tr>
		<tr>
			<td align="center">secondMax</td>
			<td>The maximum second allowed for all dates</td>
			<td align="right">59</td>
		</tr>
		<tr>
			<td align="center"><strong>Millieconds</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">millisecMin</td>
			<td>The minimum millisecond allowed for all dates</td>
			<td align="right">0</td>
		</tr>
		<tr>
			<td align="center">millisecMax</td>
			<td>The maximum millisecond allowed for all dates</td>
			<td align="right">999</td>
		</tr>
		<tr>
			<td align="center"><strong>Microsec</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">microsecMin</td>
			<td>The minimum microsecond allowed for all dates</td>
			<td align="right">0</td>
		</tr>
		<tr>
			<td align="center">microsecMax</td>
			<td>The maximum microsecond allowed for all dates</td>
			<td align="right">999</td>
		</tr>
		<tr>
			<td align="center"><strong>Date</strong></td>
			<td></td>
			<td align="right"></td>
		</tr>
		<tr>
			<td align="center">minDate</td>
			<td>Date object of the minimum datetime allowed.</td>
			<td align="right">0</td>
		</tr>
		<tr>
			<td align="center">maxDate</td>
			<td>Date object of the maximum datetime allowed.</td>
			<td align="right">31</td>
		</tr>
	</tbody>
</table>

	<?php
	Carbon_Container::factory('theme_options', __('Theme Options', 'crb'))
		->add_fields(array(
			Carbon_Field::factory('timepicker', 'time', 'Time')
				->set_restraints(array(
					'hourMin' => '9',
					'hourMax' => '18',
				)),
		));
	?>