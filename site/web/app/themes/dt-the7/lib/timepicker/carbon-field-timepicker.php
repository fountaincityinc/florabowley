<?php
/**
 * The public URL to the timepicker static files(css, js)
 */
define('TIMEPICKER_PUBLIC_URL', get_bloginfo('stylesheet_directory') . '/lib/timepicker/');


add_action( 'admin_enqueue_scripts', 'crb_enqueue_admin_scripts' );
function crb_enqueue_admin_scripts() {

	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui-datepicker');

	# Enqueue JS
	wp_enqueue_script( 'crb_jquery_timepicker', TIMEPICKER_PUBLIC_URL . '/js/jquery-ui-timepicker-addon.js', array('jquery', 'jquery-ui-datepicker', 'carbon_fields') );
	wp_enqueue_script( 'crb_timepicker', TIMEPICKER_PUBLIC_URL . '/js/carbon_field_timepicker.js', array('crb_jquery_timepicker') );

	# Enqueue CSS
	wp_enqueue_style( 'carbon-jquery-ui', TIMEPICKER_PUBLIC_URL . '/css/jquery-ui.css' );
	wp_enqueue_style( 'carbon-jquery-timepicker', TIMEPICKER_PUBLIC_URL . '/css/jquery-ui-timepicker-addon.css' );
}

/**
 * Hook field initialization
 */
add_action('after_setup_theme', 'crb_init_timepicker', 15);
function crb_init_timepicker() {
	if (class_exists("Carbon_Field")) {
		include_once dirname(__FILE__) . '/Carbon_Field_Timepicker.php';
	}
}
