jQuery(function($) {
	if (typeof carbon_field === "undefined") {
		if (typeof console !== "undefined") {
			console.log("Couldn't initialize timepicker: carbon_field variable isn't exposed to the global scope. Apply r252915 to fix this. ");
		}
		return;
	}
	/**
	 * Carbon Field Timepicker
	 */
	carbon_field.Datetime = carbon_field.Time = function(element, field_obj) {
		var $textField = element.find('.carbon-timepicker');

		if (!$textField.length) {
			return;
		}

		var dataAtts = $textField.data();
		var timepickerArgs = {};

		/* Set Timepicker Function by its type */
		var callFunction = 'timepicker';
		if ( dataAtts.hasOwnProperty('timepickerType') && dataAtts['timepickerType'] == 'datetime' ) {
			callFunction = 'datetimepicker';
		}

		/* Set Timepicker Time Format */
		timepickerArgs.timeFormat = 'HH:MM';
		if ( dataAtts.hasOwnProperty('timeFormat') ) {
			timepickerArgs.timeFormat = dataAtts['timeFormat'];
		}

		/* Set Interval Step */
		if ( dataAtts.hasOwnProperty('intervalStep') ) {
			parseDataAttributes( dataAtts['intervalStep'], timepickerArgs );
		}

		/* Set Restraints */
		if ( dataAtts.hasOwnProperty('restraints') ) {
			parseDataAttributes( dataAtts['restraints'], timepickerArgs );
		}

		$textField[callFunction](timepickerArgs);
	}

	function parseDataAttributes(value, object) {
		var output = {};
		var elements = value.split(',');

		for (var element in elements) {
			var elementDetails = elements[element].split(':');
			object[ elementDetails[0] ] = parseInt(elementDetails[1]);
		}
	}
});