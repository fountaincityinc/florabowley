var gulp = require('gulp')
var watch = require('gulp-watch')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var sourcemaps = require('gulp-sourcemaps')
// var browserSync = require('browser-sync').create()
// var reload = browserSync.reload

var del = require('del')
var svgstore = require('gulp-svgstore')
var svgmin = require('gulp-svgmin')
var rename = require('gulp-rename')

var jsPaths = {
  admin: [
    './inc/7apps/admin/js/*.js'
  ],
  seven: [
    './js/7apps/*.js'
  ],
  theme: [
    './js/main.js'
  ]
}

gulp.task('watchJs', function () {
  watch(jsPaths.theme, function () {
    gulp.start('concatThemeJs')
  })

  watch(jsPaths.seven, function () {
    gulp.start('concat7appsJs')
  })

  watch(jsPaths.admin, function () {
    gulp.start('concatAdminJs')
  })
})

gulp.task('concatThemeJs', function () {
  return gulp.src(jsPaths.theme)
    .pipe(sourcemaps.init())
    .pipe(concat('main.min.js'))
    .pipe(uglify({ /* preserveComments:'some' */ }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./js/'))
})

gulp.task('concat7appsJs', function () {
  return gulp.src(jsPaths.seven)
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(uglify({ /* preserveComments:'some' */ }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./js/7apps/build/'))
})

gulp.task('concatAdminJs', function () {
  return gulp.src(jsPaths.admin)
    .pipe(sourcemaps.init())
    .pipe(concat('admin.min.js'))
    .pipe(uglify({ /* preserveComments:'some' */ }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./inc/7apps/admin/js/build/'))
})

gulp.task('svg', function () {
  del('./svg/build/**')

  return gulp.src('./svg/*.svg')
    .pipe(svgmin())
    .pipe(rename({ prefix: 'fb-icon-' }))
    .pipe(svgstore())
    .pipe(rename({ basename: 'all.min' }))
    .pipe(gulp.dest('./svg/build/'))
})

// !-- DEFAULT ----

// gulp.task('browser-sync', function () {
//   browserSync.init({
//     proxy: 'local.flora.com',
//     port: 3000,
//     open: 'external',
//     host: 'local.flora.com',
//   })
// })

// Watch
// gulp.task('watch', function () {
//   watch([
//     './js/7apps/build/*', './js/main.min.js',
//     './inc/7apps/admin/js/build/*',
//     './**/*.php', './svg/build/**'
//   ], function () {
//     reload()
//   })
// })

gulp.task('default', [/* 'browser-sync', 'watch', */ 'watchJs', 'concat7appsJs'])
