<form class="register-form sa-form form-horizontal">
	<input type="hidden" name="redirect_to" value="<?php echo the_permalink(); ?>">
  <input type="hidden" name="action" value="register">
  <!-- <input type="hidden" name="nonce" value="<?= wp_create_nonce('user_register_nonce'); ?>"> -->

	<div class="title">
		<h1>Create Your Account</h1>
		<p class="statement">Already have an account? <a href="#login" class="toggle-tab">Login</a></p>
	</div>

	<div class="alert alert-danger"></div>
	<div class="spinner"><i class="fa fa-spinner fa-spin"></i></div>

	<div id="activation-notice" class="hidden">
		<h2 style="padding: 50px; margin: 0; font-size: font-size: 23px;">Please click the activation link we just emailed you to register for classes.</h2>
	</div>

	<fieldset>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="register-fname">First Name</label>
			<div class="col-sm-7">
				<input id="register-fname" name="fname" type="text" placeholder="" class="form-control input-md" required="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="register-lname">Last Name</label>
			<div class="col-sm-7">
				<input id="register-lname" name="lname" type="text" placeholder="" class="form-control input-md" required="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="register-email">Email</label>
			<div class="col-sm-7">
				<input id="register-email" name="email" type="text" placeholder="" class="form-control input-md" required="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="register-username">Username</label>
			<div class="col-sm-7">
				<input id="register-username" name="username" type="text" placeholder="" class="form-control input-md" required="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="register-password">Password</label>
			<div class="col-sm-7">
				<input id="register-password" name="password" type="password" placeholder="" class="form-control input-md" required="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="register-password_confirm">Password Confirmation</label>
			<div class="col-sm-7">
				<input id="register-password_confirm" name="password_confirm" type="password" placeholder="" class="form-control input-md" required="">
			</div>
		</div>

		<div class="form-group">
		  <div class="col-sm-3"></div>
		  <div class="col-sm-7">
		  	<input style="margin-left: 24px; margin-top: 13px; margin-bottom: 24px; float: right;" type="checkbox" name="optin" id="optin" value="true" checked="">
	  		<label class="control-label" style="text-align: right;" for="optin">Join my News Art Love mailing list?</label>
		  </div>
		</div>

		<div class="form-group">
			<div class="col-sm-3"></div>
			<div class="col-sm-7">
				<button class="pull-right"> Sign Up </button>
			</div>
		</div>

	</fieldset>
</form>
