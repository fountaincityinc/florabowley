<div id="sign-in-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body">
        <!-- Close Button -->
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li><a href="#register" role="tab" data-toggle="tab">Sign Up</a></li>
          <li class="active"><a href="#login" role="tab" data-toggle="tab">Login</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane" id="register">
            <!-- REGISTER -->
            <div id="register-form">
							<?php dt_get_template_part( 'modals/form', 'register' ); ?>
            </div>
            <!-- /Registration -->
          </div>

          <div class="tab-pane active" id="login">
						<?php dt_get_template_part( 'modals/form', 'sign-in' ); ?>
          </div> <!-- Login Tab -->
        </div> <!-- Tab Panes -->

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
