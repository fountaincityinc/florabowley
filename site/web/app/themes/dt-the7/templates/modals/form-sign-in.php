<form class="login-form sa-form form-horizontal">
  <input type="hidden" name="redirect_to" value="<?php the_permalink(); ?>">
  <input type="hidden" name="action" value="login">
  <!-- <input type="hidden" name="nonce" value="<?= wp_create_nonce('user_login_nonce'); ?>"> -->

  <div class="title">
    <h1>Login</h1>
    <p class="statement">Don't have an account yet? <a href="#register" class="toggle-tab">Sign Up</a></p>
  </div>

  <div class="alert alert-danger"></div>
  <div class="spinner"><i class="fa fa-spinner fa-spin"></i></div>

  <fieldset>
    <div class="form-group">
      <label class="col-sm-3 control-label" for="login-email">Username</label>
      <div class="col-sm-7">
        <input id="login-email" name="email" type="text" placeholder="or Email" class="form-control input-md" required="">
      </div>
    </div>

    <div class="form-group">
      <label class="col-sm-3 control-label" for="login-password">Password</label>
      <div class="col-sm-7">
        <input id="login-password" name="password" type="password" placeholder="" class="form-control input-md" required="">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-3"></div>
      <div class="col-sm-7">
        <button class="pull-right"> Login </button>
        <a href="/my-account/lost-password/" class="forgot">Forgot Password?</a>
      </div>
    </div>
  </fieldset>
</form>

<form class="forgot-form sa-form form-horizontal" style="display:none;">
  <input type="hidden" name="redirect_to" value="<?php the_permalink(); ?>">
  <input type="hidden" name="action" value="forgot">
  <!-- <input type="hidden" name="nonce" value="<?= wp_create_nonce('user_forgot_nonce'); ?>"> -->

  <div class="title">
    <h1> Forgot Your Password? </h1>
    <p class="statement"> Just fill in your email below to reset it. </p>
  </div>

  <div class="alert alert-danger"></div>
  <div class="spinner" style="display:none;"><i class="fa fa-spinner fa-spin"></i></div>

  <fieldset>
    <div class="form-group">
      <label class="col-sm-3 control-label" for="forgot-email">Email</label>
      <div class="col-sm-7">
        <input id="forgot-email" name="email" type="text" placeholder="" class="form-control input-md" required="">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-3"></div>
      <div class="col-sm-7">
        <button class="pull-right"> Reset Password </button>
        <a href="#" class="remember">I remember now!</a>
      </div>
    </div>
  </fieldset>
</form>
