<?php
/**
 * Portfolio post content part with rollover
 *
 * @package vogue
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$config = Presscore_Config::get_instance();

if ( $config->get( 'show_titles' ) && get_the_title() ) :
?>
	<p class="entry-title"><?php the_excerpt(); ?></p>
<?php
endif;

if ( $config->get( 'show_excerpts' ) ) {
	echo wpautop( get_the_content() );
}
?>
