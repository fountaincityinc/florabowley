<?php
	$help_link = carbon_get_post_meta( get_the_ID(), 'fb_need_help_link' );
	if ( !$help_link || strlen( $help_link ) === 0 ) { $help_link = '/support'; }

	$show_help = carbon_get_post_meta( get_the_ID(), 'fb_show_need_help' );
	if ( $show_help === 'yes' ) :
		?>
		<a href="<?= $help_link ?>?ref=support-button" class="fb-support-button">Need Help?</a>
		<?php
	endif;
?>
