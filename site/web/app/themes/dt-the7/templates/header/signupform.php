<?php
/**
 * Description here.
 *
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$show_icon = of_get_option('header-search_icon', true);
$class     = $show_icon ? '' : ' icon-off';
$caption   = of_get_option('header-search_caption', _x( "Search", "theme-options", 'the7mk2' ));

if ( !$caption && $show_icon ) {
	$class .= ' text-disable';
}

if ( !$caption ) {
	$caption = '&nbsp;';
}
?>
<div class="sa-signup">
	<form class="signupform" role="signup">
		<a href="#" class="sa-signup-open"> <i class="fa fa-heart-o"></i> <span>Stay Inspired</span> </a>
		<input type="hidden" name="action" value="mmns_submit" />
		<input
			type="text"
			class="field signupform-email"
			name="email"
			value="<?php echo esc_attr( get_search_query() ); ?>"
			placeholder="<?php _e( 'Enter your email &hellip;', 'the7mk2' ); ?>"
		/>
		<?php do_action( 'presscore_header_signupform_fields' ); ?>
		<input type="submit" class="assistive-text signupsubmit" value="<?php esc_attr_e( 'Go!', 'the7mk2' ); ?>" />
		<!-- <a href="#go" id="trigger-overlay" class="submit<?php echo $class; ?>"><?php echo esc_html($caption); ?></a> -->
	</form>
</div>
