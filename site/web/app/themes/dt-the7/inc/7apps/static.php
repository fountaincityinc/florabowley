<?php
/**
 * Frontend functions.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/////////////////////
// Enqueue scripts //
/////////////////////

if ( ! function_exists( 'sa_enqueue_scripts' ) ) :

	/**
	 * Enqueue scripts and styles.
	 */
	function sa_enqueue_scripts() {
		global $wp_styles;

		presscore_enqueue_theme_stylesheet( 'bootstrap-css', 'inc/7apps/bootstrap-compiled/css/bootstrap.min' );
		presscore_enqueue_theme_stylesheet( 'owl-carousel', 'js/plugins/owl.carousel.2.0.0/assets/owl.carousel' );
		// presscore_enqueue_theme_stylesheet( 'owl-theme', 'js/plugins/owl.carousel.2.0.0/assets/owl.theme.default.min' );

		presscore_enqueue_theme_script( 'bootstrap-js', 'inc/7apps/bootstrap-compiled/js/bootstrap.min', array( 'jquery' ) );
		presscore_enqueue_theme_script( 'jquery-mousewheel', 'js/plugins/jquery.mousewheel.min', array( 'jquery' ) );
		// presscore_enqueue_theme_script( 'froogaloop', 'js/plugins/froogaloop.min', array( 'jquery' ) );
		presscore_enqueue_theme_script( 'vimeo-player', 'js/plugins/vimeo-player.2.2.0.min', array() );
		presscore_enqueue_theme_script( 'owl-carousel', 'js/plugins/owl.carousel.2.0.0/owl.carousel', array( 'jquery' ) );
		presscore_enqueue_theme_script( 'transition-end', 'js/plugins/transition-end', array( 'jquery' ) );

		// Moved to minified app JS
		presscore_enqueue_theme_script( 'sa-app', 'js/7apps/build/app.min', array( 'jquery', 'underscore', 'transition-end', 'owl-carousel', 'jquery-mousewheel', 'vimeo-player', 'bootstrap-js' ), filemtime( PRESSCORE_THEME_DIR . '/js/7apps/build/app.min.js' ), true );

		$saGlobal = array(
			'site_url' => site_url(),
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script( 'sa-app', 'saGlobal', $saGlobal );
	}

endif; // sa_enqueue_scripts

add_action( 'wp_enqueue_scripts', 'sa_enqueue_scripts', 15 );

if ( ! function_exists( 'sa_admin_enqueue_scripts' ) ) :

	/**
	 * Enqueue scripts and styles.
	 */
	function sa_admin_enqueue_scripts() {
		global $wp_styles;

		$theme_version = wp_get_theme()->get( 'Version' );

		presscore_enqueue_theme_stylesheet( 'font-awesome', 'bower_components/font-awesome/css/font-awesome.min' );
		// presscore_enqueue_theme_stylesheet( 'sa-admin-css', 'css/7apps/admin' );

		presscore_enqueue_theme_script( 'moment', 'bower_components/moment/min/moment.min', array(), $theme_version, true );
		presscore_enqueue_theme_script( 'handlebars', 'bower_components/handlebars/handlebars.min', array(), $theme_version, true );
		presscore_enqueue_theme_script( 'sa-admin-app', 'inc/7apps/admin/js/build/admin.min', array( 'jquery', 'underscore', 'handlebars', 'moment' ), filemtime( SEVEN_APPS_DIR . '/admin/js/build/admin.min.js' ), true );

		$saGlobal = array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script( 'sa-admin-app', 'saGlobal', $saGlobal );
	}

endif; // sa_admin_enqueue_scripts

add_action( 'admin_enqueue_scripts', 'sa_admin_enqueue_scripts', 15 );

function go_home() {
	$r = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : home_url();
	wp_redirect( $r );
	exit();
}

add_action( 'wp_logout', 'go_home' );

function remove_admin_bar() {
	if ( !current_user_can('administrator') && !is_admin() ) {
		show_admin_bar( false );
	}
}

add_action( 'after_setup_theme', 'remove_admin_bar' );
