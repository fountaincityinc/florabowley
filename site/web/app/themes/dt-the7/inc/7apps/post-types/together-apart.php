<?php
/**
 * Brave Intuitive TogetherApart custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// TogetherApart PostType
/*******************************************************************/

if ( ! class_exists('Flora_TogetherApart_PostType') ):

class Flora_TogetherApart_PostType {
	public static $post_type     = 'fb_together_apart';
	// public static $taxonomy      = 'fb_together_apart_category';
	public static $menu_position = 36;

	public static function init() {
		self::register();
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Together Apart', 'fb' ),
			'singular_name'      => __( 'Together Apart', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Page', 'fb' ),
			'edit_item'          => __( 'Edit Together Apart Page', 'fb' ),
			'new_item'           => __( 'New Together Apart Page', 'fb' ),
			'view_item'          => __( 'View Together Apart Page', 'fb' ),
			'search_items'       => __( 'Search Together Apart Pages', 'fb' ),
			'not_found'          => __( 'No Together Apart pages found', 'fb' ),
			'not_found_in_trash' => __( 'No Together Apart pages found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Together Apart', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'together-apart-replay' ),
			'capability_type'    => 'page',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-desktop',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author', 'page-attributes' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		// $labels = array(
		// 	'name'              => __( 'Together Apart Categories', 'fb' ),
		// 	'singular_name'     => __( 'Together Apart Category', 'fb' ),
		// 	'search_items'      => __( 'Search in Category', 'fb' ),
		// 	'all_items'         => __( 'Together Apart Categories', 'fb' ),
		// 	'parent_item'       => __( 'Parent Together Apart Category', 'fb' ),
		// 	'parent_item_colon' => __( 'Parent Together Apart Category:', 'fb' ),
		// 	'edit_item'         => __( 'Edit Category', 'fb' ),
		// 	'update_item'       => __( 'Update Category', 'fb' ),
		// 	'add_new_item'      => __( 'Add New Together Apart Category', 'fb' ),
		// 	'new_item_name'     => __( 'New Category Name', 'fb' ),
		// 	'menu_name'         => __( 'Together Apart Categories', 'fb' )
		// );
		//
		// $taxonomy_args = array(
		// 	'hierarchical'      => true,
		// 	'public'            => true,
		// 	'labels'            => $labels,
		// 	'show_ui'           => true,
		// 	'rewrite'           => array( 'slug' => 'together-apart-replay-category' ),
		// 	'show_admin_column' => true,
		// );

		// $taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		// register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
