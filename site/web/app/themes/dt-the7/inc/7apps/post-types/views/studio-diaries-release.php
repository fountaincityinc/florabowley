<div class="wrap">
	<div class="shell">
		<h2> Release Studio Diary </h2>

		<form method="post">
			<input type="hidden" name="nonce" value="<?= wp_create_nonce('diaries_release_nonce'); ?>">

			<label> Select month and year to release: </label>
			<select id="month" name="month">
				<option value="01" <?php if ( date('m') == '01' ) { echo 'selected'; } ?>> Jan </option>
				<option value="02" <?php if ( date('m') == '02' ) { echo 'selected'; } ?>> Feb </option>
				<option value="03" <?php if ( date('m') == '03' ) { echo 'selected'; } ?>> Mar </option>
				<option value="04" <?php if ( date('m') == '04' ) { echo 'selected'; } ?>> Apr </option>
				<option value="05" <?php if ( date('m') == '05' ) { echo 'selected'; } ?>> May </option>
				<option value="06" <?php if ( date('m') == '06' ) { echo 'selected'; } ?>> Jun </option>
				<option value="07" <?php if ( date('m') == '07' ) { echo 'selected'; } ?>> Jul </option>
				<option value="08" <?php if ( date('m') == '08' ) { echo 'selected'; } ?>> Aug </option>
				<option value="09" <?php if ( date('m') == '09' ) { echo 'selected'; } ?>> Sep </option>
				<option value="10" <?php if ( date('m') == '10' ) { echo 'selected'; } ?>> Oct </option>
				<option value="11" <?php if ( date('m') == '11' ) { echo 'selected'; } ?>> Nov </option>
				<option value="12" <?php if ( date('m') == '12' ) { echo 'selected'; } ?>> Dec </option>
			</select>
			<select id="year" name="year">
				<option value="2015" <?php if ( date('Y') == '2015' ) { echo 'selected'; } ?>> 2015 </option>
				<option value="2016" <?php if ( date('Y') == '2016' ) { echo 'selected'; } ?>> 2016 </option>
				<option value="2017" <?php if ( date('Y') == '2017' ) { echo 'selected'; } ?>> 2017 </option>
				<option value="2018" <?php if ( date('Y') == '2018' ) { echo 'selected'; } ?>> 2018 </option>
				<option value="2019" <?php if ( date('Y') == '2019' ) { echo 'selected'; } ?>> 2019 </option>
			</select>

			<input type="submit" value="Release" class="button button-primary button-large">
		</form>

		<p>
			Please be patient after submitting this form, this could take a while to process.
			<br> Only press submit once and do not refresh the page.
			<br> A count of unlocks will be displayed once this is finished processing.
		</p>

		<?php if ( !empty( $_POST ) ) : ?>
			<hr>

			<p> Adding <strong><?php echo date( 'F Y', strtotime( $_POST['year'] . $_POST['month'] . '01' ) ); ?></strong> Studio Diary to Yearly and Monthly Customer Accounts. </p>

			<?php
				$locked = 0; $unlocked = 0;
				foreach ( $yearly_unlocks as $status ) {
					if ( !$status ) { $locked++; } else { $unlocked++; }
				}
			?>
			<p> <?php echo $unlocked; ?> <strong>Yearly</strong> Customers unlocked. </p>

			<?php
				$locked = 0; $unlocked = 0;
				foreach ( $monthly_unlocks as $status ) {
					if ( !$status ) { $locked++; } else { $unlocked++; }
				}
			?>
			<p> <?php echo $unlocked; ?> <strong>Monthly</strong> Customers unlocked. </p>
		<?php endif; ?>
	</div>
</div>
