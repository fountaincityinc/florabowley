<?php
/**
 * Brave Intuitive CreativeWellness custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// CreativeWellness PostType
/*******************************************************************/

if ( ! class_exists('Flora_CreativeWellness_PostType') ):

class Flora_CreativeWellness_PostType {
	public static $post_type     = 'fb_creative_wellness';
	// public static $taxonomy      = 'fb_creative_wellness_category';
	public static $menu_position = 36;

	public static function init() {
		self::register();
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Creative Wellness', 'fb' ),
			'singular_name'      => __( 'Creative Wellness', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Page', 'fb' ),
			'edit_item'          => __( 'Edit Creative Wellness Page', 'fb' ),
			'new_item'           => __( 'New Creative Wellness Page', 'fb' ),
			'view_item'          => __( 'View Creative Wellness Page', 'fb' ),
			'search_items'       => __( 'Search Creative Wellness Pages', 'fb' ),
			'not_found'          => __( 'No Creative Wellness pages found', 'fb' ),
			'not_found_in_trash' => __( 'No Creative Wellness pages found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Creative Wellness', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'creative-wellness' ),
			'capability_type'    => 'page',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-desktop',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author', 'page-attributes' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		// $labels = array(
		// 	'name'              => __( 'Creative Wellness Categories', 'fb' ),
		// 	'singular_name'     => __( 'Creative Wellness Category', 'fb' ),
		// 	'search_items'      => __( 'Search in Category', 'fb' ),
		// 	'all_items'         => __( 'Creative Wellness Categories', 'fb' ),
		// 	'parent_item'       => __( 'Parent Creative Wellness Category', 'fb' ),
		// 	'parent_item_colon' => __( 'Parent Creative Wellness Category:', 'fb' ),
		// 	'edit_item'         => __( 'Edit Category', 'fb' ),
		// 	'update_item'       => __( 'Update Category', 'fb' ),
		// 	'add_new_item'      => __( 'Add New Creative Wellness Category', 'fb' ),
		// 	'new_item_name'     => __( 'New Category Name', 'fb' ),
		// 	'menu_name'         => __( 'Creative Wellness Categories', 'fb' )
		// );
		//
		// $taxonomy_args = array(
		// 	'hierarchical'      => true,
		// 	'public'            => true,
		// 	'labels'            => $labels,
		// 	'show_ui'           => true,
		// 	'rewrite'           => array( 'slug' => 'creative-wellness-category' ),
		// 	'show_admin_column' => true,
		// );

		// $taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		// register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
