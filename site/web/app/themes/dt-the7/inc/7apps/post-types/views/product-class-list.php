<div class="wrap">
	<div class="shell">
		<h2> Attendee List </h2>

		<div id="product-attendee-list">
			<p> Select a Product:
				<select class="product-list">
						<option value=""></option>
					<?php foreach ( $products as $id => $name ) : ?>
						<option value="<?php echo $id; ?>"> <?php echo $name ?> </option>
					<?php endforeach; ?>
				</select>
			</p>

			<table id="product-attendee-list-table" cellpadding=0 cellspacing=0 class="wp-list-table widefat fixed striped posts">
				<thead>
					<tr>
						<td style="width: 50px;"> ID </td>
						<td> First </td>
						<td> Last </td>
						<td> Email </td>
						<td> Phone </td>
						<td> Address </td>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td colspan=6> Select a product above. </td>
					</tr>

					<script id="product-attendee-list-row" type="text/x-handlebars-template">
						<tr>
							<td> <a href="/wp-admin/post.php?post={{order_id}}&action=edit" target="_blank">{{order_id}}</a> </td>
							<td> {{first_name}} </td>
							<td> {{last_name}} </td>
							<td> <a href="mailto:{{email}}"> {{email}} </a> </td>
							<td> {{formattedPhone phone}} </td>
							<td> {{{formattedAddress this}}} </td>
						</tr>
					</script>

					<script id="product-attendee-list-not-found" type="text/x-handlebars-template">
						<tr>
							<td colspan=6> No attendees could be found. </td>
						</tr>
					</script>
				</tbody>

				<tfoot>
					<tr>
						<td> ID </td>
						<td> First </td>
						<td> Last </td>
						<td> Email </td>
						<td> Phone </td>
						<td> Address </td>
					</tr>
				</tfoot>
			</table>
		</div>

	</div>
</div>
