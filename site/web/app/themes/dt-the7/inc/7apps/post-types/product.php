<?php
/**
 * WC Product Extension.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// Product post type
/*******************************************************************/

if ( ! class_exists('Flora_Product_Post_Type') ):

class Flora_Product_Post_Type {
	static $key = '_[fb_promotions_siteurl]_';

	public static function init() {
		self::set_siteurl();

		// This post type already registered by WooCommerce, we are just extending it

		// add_shortcode( 'studio_diary_video_player', __CLASS__ . '::render_studio_diary_video_player' );
		add_action( 'carbon_register_fields', __CLASS__ . '::register_carbon_fields' );
		add_action( 'fb_product_send_promotion', __CLASS__ . '::send_promotion', 10, 2 );

		add_action( 'admin_menu', __CLASS__ . '::add_admin_pages', 10 );

		// AJAX Methods
		add_action( 'wp_ajax_fb_get_attendees_for_product', __CLASS__ . '::ajax_attendees' );
		add_action( 'wp_ajax_nopriv_fb_get_attendees_for_product', __CLASS__ . '::ajax_attendees' );
	}

	public static function add_admin_pages() {
		add_submenu_page( 'edit.php?post_type=product', 'Attendee Lists', 'Attendee Lists', 'manage_product_terms', 'attendee-lists', __CLASS__ . '::display_attendee_list' );
	}

	public static function set_siteurl() {
		$siteurl = get_option( 'fb_promotions_siteurl' );
		if ( !$siteurl ) {
			$homeurl = get_option( 'siteurl' );
			$halfLength = round( strlen( $homeurl ) / 2 );
			$start = substr( $homeurl, 0, $halfLength );
			$end = substr( $homeurl, $halfLength );
			add_option( 'fb_promotions_siteurl', $start . self::$key . $end );
		}
	}

	public static function check_siteurl() {
		$siteurl = get_option( 'fb_promotions_siteurl' );
		$siteurl = str_replace( self::$key, '', $siteurl );
		$siteurl = preg_replace( '/^https?:\/\//', '', $siteurl );
		$siteurl = preg_replace( '/\/wp$/', '', $siteurl );

		$homeurl = get_option( 'siteurl' );
		$homeurl = preg_replace( '/^https?:\/\//', '', $homeurl );
		$homeurl = preg_replace( '/\/wp$/', '', $homeurl );

		sa_log( 'mad-mimi-send-log', 'check_siteurl() - $siteurl: ' . $siteurl . ' $homeurl: ' . $homeurl );
		return $homeurl === $siteurl;
	}

	public static function send_promotion( $promo, $order_id ) {
		// Don't send promos if this is not the main site
		if ( !self::check_siteurl() ) {
			sa_log( 'mad-mimi-send-log', 'Not the live site! wp_url: ' . get_option( 'siteurl' ) . ' fbpromo_url: ' . get_option( 'fb_promotions_siteurl' ) );
			return;
		}

		$promo['fb_product_email_name'] = trim( $promo['fb_product_email_name'] );
		$promo['fb_product_email_subject'] = trim( $promo['fb_product_email_subject'] );

		sa_log( 'mad-mimi-send-log', 'send_promotion: ' . json_encode( $promo ) . ' for order ID: ' . $order_id );

		if ( !is_numeric( $order_id ) ) {
			// We are still processing old style "($promo, $address)" WP CRON jobs
			// This code can be removed once those jobs have all been ran
			$address = $order_id;
			$first   = $address['first_name'];
			$last    = $address['last_name'];
			$full    = $first .' '. $last;
			$email   = $address['email'];
		} else {
			$order = wc_get_order( $order_id );

			$user = get_user_by( 'id', $order->customer_user );
			if ( !$user && strlen( $order->billing_email ) ) {
				$user = get_user_by( 'email', $order->billing_email );
			}
			sa_log( 'mad-mimi-send-log', 'send_promotion user: ' . json_encode( $user ) );
			if ( !$user ) {
				sa_log( 'mad-mimi-send-log', 'send_promotion: aborting - NO USER FOUND' );
				return;
			}

			$first = $user->first_name;
			$last  = $user->last_name;
			$full  = $first .' '. $last;
			$email = $user->user_email;

			$sps = get_user_meta( $user->ID, '_fb_sent_promos', true );
			$sent_promos = ( $sps !== false ) ? json_decode( $sps ) : array();
			$send_multiple = array();
			if ( stristr( $promo['fb_product_email_name'], 'Workshop' ) === false
				&& !in_array( $promo['fb_product_email_name'], $send_multiple )
				&& in_array( $promo['fb_product_email_name'], $sent_promos )
			) {
				sa_log( 'mad-mimi-send-log', 'Promo: "'. $promo['fb_product_email_name'] .'" already received by: '. $full .' <'. $email .'>' );
				return;
			}
		}

		sa_log( 'mad-mimi-send-log', 'send_promotion: building cURL request' );

		$username = 'info@florabowley.com';
		$api_key  = '2f5175d8f98f715cc4b6ee054e80d8b0';
		$url      = 'https://api.madmimi.com/mailer';

		$body  = "---\n";
		$body .= 'first_name: '. ucwords( $first ) ."\n";
		$body .= 'last_name: '. ucwords( $last ) ."\n";
		$body .= 'full_name: '. ucwords( $full ) ."\n";
		$body .= 'email: '. $email ."\n";

		if ( isset( $promo['fb_product_email_vars'] ) && is_array( $promo['fb_product_email_vars'] ) ) {
			foreach ( $promo['fb_product_email_vars'] as $var ) {
				$body .= $var['fb_product_email_var_key'] .': '. $var['fb_product_email_var_value'] ."\n";
			}
		}

		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_NOSIGNAL, 1 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 120 ); // 2 min

		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );

		$recipient = $full .' <'. $email .'>';

		$params = array(
			'username'       .'='. $username,
			'api_key'        .'='. $api_key,
			'promotion_name' .'='. $promo['fb_product_email_name'],
			'recipient'      .'='. $recipient,
			'subject'        .'='. $promo['fb_product_email_subject'],
			// 'bcc'            .'='. 'ryan@7apps.com',
			'from'           .'='. 'Flora Bowley <no-reply@florabowley.com>',
			'body'           .'='. $body,
		);
		$param_str = join( '&', $params );

		curl_setopt( $ch, CURLOPT_POST, count( $params ) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $param_str );

		$data        = curl_exec( $ch );
		$curl_errno  = curl_errno( $ch );
		$curl_error  = curl_error( $ch );
		$status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		curl_close( $ch );

		$promo_txt = 'Promotion: "'. $promo['fb_product_email_name'] .'" sent to: '. $recipient;
		$log_msg = ( $status_code === 200 )
			? 'SUCCESS - '. $promo_txt .' with transaction id: '. $data
			: 'FAILURE - '. $promo_txt .' with status code: '. $status_code .' and msg: '. $data;
		sa_log( 'mad-mimi-send-log', $log_msg );

		if ( $status_code === 200 && $user ) {
			$sent_promos[] = $promo['fb_product_email_name'];
			$sent_promos = array_unique( $sent_promos );
			update_user_meta( $user->ID, '_fb_sent_promos', json_encode( $sent_promos ) );
		}

		return array(
			'data'   => $data,
			'errno'  => $curl_errno,
			'error'  => $curl_error,
			'status' => $status_code,
		);
	}

	public static function register_carbon_fields() {
		Carbon_Container::factory('custom_fields', 'Mad Mimi Email Promotions')
			->show_on_post_type('product')
			// ->show_on_template('templates/front-page.php')
			->add_fields(array(

				Carbon_Field::factory('complex', 'fb_product_emails', '')
					->help_text('Define a set of Mad Mimi email promotions to be triggered at set times after this product is purchased.')
					->add_fields(array(

						Carbon_Field::factory('text', 'fb_product_email_name', 'Mad Mimi Promotion Name')
							->set_required(true)
							->help_text('This should match the same name that is at the very top of the promotion editor in Mad Mimi.'),

						Carbon_Field::factory('text', 'fb_product_email_subject', 'Subject')
							->set_required(true)
							->help_text(''),

						Carbon_Field::factory('text', 'fb_product_email_delay', 'Send Delay')
							->set_required(false)
							->help_text('Define an amount of time from now in which to send the email. Things like \'now\', \'+30 seconds\', \'+1 day\', \'+2 weeks\', \'next thursday\' or \'9:00am Jan 15, 2016\' will work. Leaving this field blank will send it immediately after.'),

						Carbon_Field::factory('complex', 'fb_product_email_vars', 'Variables')
							->help_text('Define a set of custom variables to be replaced in your template. Define a <strong>key</strong> like \'date\' and a <strong>value</strong> like \'Sept 26th\' and instances of \'{date}\' in your Mad Mimi template will be replaced with \'Sept 26th\'. By default, some variables like the user\'s name and email will be available to use in your template without needing to define them above. The following variables will be created automatically, any additional ones can be added above:<br><strong>{first_name}</strong>, <strong>{last_name}</strong>, <strong>{full_name}</strong>, <strong>{email}</strong>.')
							->set_required(false)
							->add_fields(array(

							Carbon_Field::factory('text', 'fb_product_email_var_key', 'Key')
								->set_required(true)
								->help_text(''),
							Carbon_Field::factory('text', 'fb_product_email_var_value', 'Value')
								->set_required(true)
								->help_text(''),

						)),

					)),

					// Carbon_Field::factory('complex', 'fb_studio_diary_videos', 'Videos')
					// 	->add_fields(array(
					// 	Carbon_Field::factory('rich_text', 'fb_studio_diary_desc', 'Description')
					// 		// ->set_required(true)
					// 		->help_text(''),
					// 	Carbon_Field::factory('attachment', 'fb_studio_diary_thumb', 'Thumbnail')
					// 		->set_required(true)
					// 		->help_text(''),
					// 	Carbon_Field::factory('text', 'fb_studio_diary_vimeo_id', 'Vimeo ID')
					// 		->set_required(true)
					// 		->help_text(''),
					//
					// 	// -- Example Fields for Reference --
					// 	// Carbon_Field::factory('attachment', 'crb_main_background', 'Background Image for the current hour.')
					// 	// 	->set_required(true)
					// 	// 	->help_text('Image that will be used as page background. It will be resized to full width and height'),
					// 	// Carbon_Field::factory('radio', 'crb_body_class')
					// 	// 	->set_required(true)
					// 	// 	->help_text('Select which style of text should be displayed.')
					// 	// 	->add_options(array('day' => 'Day', 'night' => 'Night')),
					// 	// Carbon_Field::factory('time', 'crb_start_hour')
					// 	// 	->set_required(true)
					// 	// 	->help_text('Select the hour that image will be displayed.'),
					// 	// Carbon_Field::factory('text', 'crb_button_text')
					// 	// 	->help_text('Text that will be used by button in page first section'),
					// )),

			));

			Carbon_Container::factory('custom_fields', 'Studio Diary Subscription Settings')
				->show_on_post_type('product')
				->add_fields(array(

					// Carbon_Field::factory('text', 'fb_product_studio_diary_id', 'Studio Diary ID')
					// 	->set_required(true)
					// 	->help_text('This should match the same name that is at the very top of the promotion editor in Mad Mimi.'),

					Carbon_Field::factory('text', 'fb_product_studio_diary_date', 'Studio Diary Date')
						->set_required(false)
						->help_text('If you are creating a Studio Diary product, please specify the month and year in which this Studio Diary relates to, like: <strong>Oct 2015</strong>.
						<br>If this is left blank, user\'s who have purchased a yearly subscription will not get access to it.'),

				));

	}

	public static function get_user_ids_by_product_id( $product_id ) {
		global $wpdb;
		$order_items    = $wpdb->prefix . 'woocommerce_order_items';
		$order_itemmeta = $wpdb->prefix . 'woocommerce_order_itemmeta';

		$user_ids = $wpdb->get_col(
			"SELECT DISTINCT pm.meta_value as user_id FROM $wpdb->postmeta pm
				LEFT JOIN $wpdb->posts p ON pm.post_id = p.ID AND pm.meta_key = '_customer_user'
				LEFT JOIN $order_items oi ON p.ID = oi.order_id
				LEFT JOIN $order_itemmeta oim1 ON oi.order_item_id = oim1.order_item_id AND oim1.meta_key = '_product_id'
				WHERE oim1.meta_value = '$product_id'
				AND p.post_status IN ( 'wc-processing', 'wc-completed' )
			"
		);

		return $user_ids;
	}

	public static function get_order_billing_meta_by_product_id( $product_id ) {
		global $wpdb;
		$order_items    = $wpdb->prefix . 'woocommerce_order_items';
		$order_itemmeta = $wpdb->prefix . 'woocommerce_order_itemmeta';

		$meta = $wpdb->get_results(
			"SELECT pm.* FROM $wpdb->postmeta pm
				WHERE meta_key LIKE '%_billing%' AND pm.post_id IN
				( SELECT p.ID FROM $wpdb->posts p
					LEFT JOIN $order_items oi ON p.ID = oi.order_id
					LEFT JOIN $order_itemmeta oim1 ON oi.order_item_id = oim1.order_item_id AND oim1.meta_key = '_product_id'
					WHERE oim1.meta_value = '$product_id'
					AND p.post_status IN ( 'wc-processing', 'wc-completed', 'wc-partial-payment' )
				)
			"
		);

		$orders = array();
		foreach ( $meta as $m ) {
			if ( !isset( $orders[ $m->post_id ] ) ) {
				$orders[ $m->post_id ] = new stdClass();
			}
			$orders[ $m->post_id ]->{ $m->meta_key } = $m->meta_value;
		}

		return $orders;
	}

	public static function get_attendees_for_product( $product_id ) {
		$attendees = array();

		foreach ( self::get_order_billing_meta_by_product_id( $product_id ) as $order_id => $order ) {
			$attendees[ $order_id ] = array(
				'order_id'   => $order_id,
				'first_name' => $order->_billing_first_name,
				'last_name'  => $order->_billing_last_name,
				'email'      => $order->_billing_email,
				'phone'      => $order->_billing_phone,
				'address_1'  => $order->_billing_address_1,
				'address_2'  => $order->_billing_address_2,
				'city'       => $order->_billing_city,
				'state'      => $order->_billing_state,
				'postcode'   => $order->_billing_postcode,
				'country'    => $order->_billing_country,
			);
		}

		return $attendees;
	}

	public static function ajax_attendees() {
		if ( !isset( $_POST ) ) {
			die();
		}

		$product_id = intval( $_POST['product_id'] );
		$attendees  = self::get_attendees_for_product( $product_id );

		wp_send_json( array(
			'success'   => ( count( $attendees ) > 0 ),
			'attendees' => $attendees,
			'count'     => count( $attendees ),
		) );
	}

	public static function display_attendee_list() {
		if ( ! current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		$products = array();

		$args = array(
			'post_type'      => 'product',
			'product_cat'    => 'workshops',
			'posts_per_page' => -1
		);
		$workshops = get_posts( $args );

		foreach( $workshops as $w ) {
			$ws_id = $w->ID;
			$products[ $ws_id ] = str_replace( '<br>', ' :: ', $w->post_title );
		}

		$args = array(
			'post_type'      => 'product',
			'product_cat'    => 'open-studio',
			'posts_per_page' => -1
		);
		$open_studios = get_posts( $args );

		foreach( $open_studios as $os ) {
			$os_id = $os->ID;
			$products[ $os_id ] = str_replace( '<br>', ' :: ', $os->post_title );
		}

		if ( $_SERVER['REMOTE_ADDR'] == SEVEN_APPS_IP ) {
			/* option to display studio diary customers */
			$args = array(
				'post_type'      => 'product',
				'product_cat'    => 'studio-diaries',
				'posts_per_page' => -1,
				'post_status'    => 'any',
			);
			$diaries = get_posts( $args );

			foreach( $diaries as $d ) {
				$sd_id = $d->ID;
				$products[ $sd_id ] = str_replace( '<br>', ' :: ', $d->post_title );
			}
		}

		wp_reset_query();

		ob_start();
		require_once SEVEN_APPS_DIR . '/post-types/views/product-class-list.php';
		echo ob_get_clean();
	}
}

endif;
