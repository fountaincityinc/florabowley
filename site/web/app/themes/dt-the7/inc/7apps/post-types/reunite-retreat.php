<?php
/**
 * Reunite Retreat custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// ReuniteRetreat PostType
/*******************************************************************/

if ( ! class_exists('Flora_ReuniteRetreat_PostType') ):

class Flora_ReuniteRetreat_PostType {
	public static $post_type     = 'fb_reunite_retreat';
	// public static $taxonomy      = 'fb_reunite_retreat_category';
	public static $menu_position = 36;

	public static function init() {
		self::register();
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Reunite Retreat', 'fb' ),
			'singular_name'      => __( 'Reunite Retreat', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Page', 'fb' ),
			'edit_item'          => __( 'Edit Reunite Retreat Page', 'fb' ),
			'new_item'           => __( 'New Reunite Retreat Page', 'fb' ),
			'view_item'          => __( 'View Reunite Retreat Page', 'fb' ),
			'search_items'       => __( 'Search Reunite Retreat Pages', 'fb' ),
			'not_found'          => __( 'No Reunite Retreat pages found', 'fb' ),
			'not_found_in_trash' => __( 'No Reunite Retreat pages found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Reunite Retreat', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'reunite-retreat' ),
			'capability_type'    => 'page',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-desktop',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author', 'page-attributes' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		// $labels = array(
		// 	'name'              => __( 'Reunite Retreat Categories', 'fb' ),
		// 	'singular_name'     => __( 'Reunite Retreat Category', 'fb' ),
		// 	'search_items'      => __( 'Search in Category', 'fb' ),
		// 	'all_items'         => __( 'Reunite Retreat Categories', 'fb' ),
		// 	'parent_item'       => __( 'Parent Reunite Retreat Category', 'fb' ),
		// 	'parent_item_colon' => __( 'Parent Reunite Retreat Category:', 'fb' ),
		// 	'edit_item'         => __( 'Edit Category', 'fb' ),
		// 	'update_item'       => __( 'Update Category', 'fb' ),
		// 	'add_new_item'      => __( 'Add New Reunite Retreat Category', 'fb' ),
		// 	'new_item_name'     => __( 'New Category Name', 'fb' ),
		// 	'menu_name'         => __( 'Reunite Retreat Categories', 'fb' )
		// );
		//
		// $taxonomy_args = array(
		// 	'hierarchical'      => true,
		// 	'public'            => true,
		// 	'labels'            => $labels,
		// 	'show_ui'           => true,
		// 	'rewrite'           => array( 'slug' => 'reunite-retreat-category' ),
		// 	'show_admin_column' => true,
		// );

		// $taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		// register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
