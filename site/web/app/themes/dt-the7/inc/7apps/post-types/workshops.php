<?php
/**
 * Workshops custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// Workshops post type
/*******************************************************************/

if ( ! class_exists('Flora_Workshops_Post_Type') ):

class Flora_Workshops_Post_Type {
	public static $post_type     = 'fb_workshops';
	// public static $taxonomy      = 'fb_workshops_category';
	public static $menu_position = 35;

	public static function init() {
		self::register();

		add_filter( 'manage_'. self::$post_type .'_posts_columns', __CLASS__ . '::set_workshop_columns' );
		add_action( 'manage_'. self::$post_type .'_posts_custom_column', __CLASS__ . '::custom_workshop_column', 10, 2 );
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => _x( 'Workshops',                   'backend workshops', 'fb' ),
			'singular_name'      => _x( 'Workshops',                   'backend workshops', 'fb' ),
			'add_new'            => _x( 'Add New',                     'backend workshops', 'fb' ),
			'add_new_item'       => _x( 'Add New Workshop',            'backend workshops', 'fb' ),
			'edit_item'          => _x( 'Edit Workshop',               'backend workshops', 'fb' ),
			'new_item'           => _x( 'New Workshop',                'backend workshops', 'fb' ),
			'view_item'          => _x( 'View Workshop',               'backend workshops', 'fb' ),
			'search_items'       => _x( 'Search Workshops',            'backend workshops', 'fb' ),
			'not_found'          => _x( 'No workshops found',          'backend workshops', 'fb' ),
			'not_found_in_trash' => _x( 'No workshops found in Trash', 'backend workshops', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => _x( 'Workshops',                   'backend workshops', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'classes' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => self::$menu_position,
			'supports'           => array( 'title', 'revisions', 'author' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		// $labels = array(
		// 	'name'              => _x( 'Workshop Categories',       'backend workshops', 'fb' ),
		// 	'singular_name'     => _x( 'Workshop Category',         'backend workshops', 'fb' ),
		// 	'search_items'      => _x( 'Search in Category',        'backend workshops', 'fb' ),
		// 	'all_items'         => _x( 'Workshop Categories',       'backend workshops', 'fb' ),
		// 	'parent_item'       => _x( 'Parent Workshop Category',  'backend workshops', 'fb' ),
		// 	'parent_item_colon' => _x( 'Parent Workshop Category:', 'backend workshops', 'fb' ),
		// 	'edit_item'         => _x( 'Edit Category',             'backend workshops', 'fb' ),
		// 	'update_item'       => _x( 'Update Category',           'backend workshops', 'fb' ),
		// 	'add_new_item'      => _x( 'Add New Workshop Category', 'backend workshops', 'fb' ),
		// 	'new_item_name'     => _x( 'New Category Name',         'backend workshops', 'fb' ),
		// 	'menu_name'         => _x( 'Workshop Categories',       'backend workshops', 'fb' ),
		// );
		//
		// $taxonomy_args = array(
		// 	'hierarchical'      => true,
		// 	'public'            => true,
		// 	'labels'            => $labels,
		// 	'show_ui'           => true,
		// 	'rewrite'           => array( 'slug' => 'workshop-category' ),
		// 	'show_admin_column' => true,
		// );
		//
		// $taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );
		//
		// register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}

	public static function set_workshop_columns( $columns ) {
		unset( $columns['author'] );
		unset( $columns['date'] );

		$columns['workshop_date'] = __( 'Date', 'fb' );
		$columns['button_shortcode'] = __( 'Register Button Shortcode', 'fb' );

		return $columns;
	}

	public static function custom_workshop_column( $column, $post_id ) {
		switch ( $column ) {

			case 'button_shortcode':
				// btn_register_workshop shortcode found below
				// defined in: ./classes/shopping-cart.php
				?>
				<div class="js-copy-wrap">
					<input type="text"
						value="[btn_register_workshop id=&quot;<?php echo $post_id; ?>&quot; text=&quot;Register Now&quot;]"
						readonly="readonly"
						style="width: 380px; max-width: 90%;" />
					<a href="#copy" class="js-copy"> <i class="fa fa-copy"></i> </a>
				</div>
				<?php
				break;

			case 'workshop_date':
				echo get_post_meta( $post_id, '_fb_workshop_options_date', true );
				break;
		}
	}
}

endif;
