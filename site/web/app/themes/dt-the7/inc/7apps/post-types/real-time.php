<?php
/**
 * Real Time custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// Real Time post type
/*******************************************************************/

if ( ! class_exists('Flora_Real_Time_Post_Type') ):

class Flora_Real_Time_Post_Type {
	public static $post_type     = 'fb_realtime';
	public static $taxonomy      = 'fb_realtime_category';
	public static $menu_position = 37;

	public static function init() {
		self::register();
		add_image_size( 'realtime-thumb', 640, 640, true );
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Real Time', 'fb' ),
			'singular_name'      => __( 'Real Time', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Real Time', 'fb' ),
			'edit_item'          => __( 'Edit Real Time', 'fb' ),
			'new_item'           => __( 'New Real Time', 'fb' ),
			'view_item'          => __( 'View Real Time', 'fb' ),
			'search_items'       => __( 'Search Real Time', 'fb' ),
			'not_found'          => __( 'No real times found', 'fb' ),
			'not_found_in_trash' => __( 'No real times found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Real Time', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'realtime' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-art',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		$labels = array(
			'name'              => __( 'Real Time Categories', 'fb' ),
			'singular_name'     => __( 'Real Time Category', 'fb' ),
			'search_items'      => __( 'Search in Category', 'fb' ),
			'all_items'         => __( 'Real Time Categories', 'fb' ),
			'parent_item'       => __( 'Parent Real Time Category', 'fb' ),
			'parent_item_colon' => __( 'Parent Real Time Category:', 'fb' ),
			'edit_item'         => __( 'Edit Category', 'fb' ),
			'update_item'       => __( 'Update Category', 'fb' ),
			'add_new_item'      => __( 'Add New Real Time Category', 'fb' ),
			'new_item_name'     => __( 'New Category Name', 'fb' ),
			'menu_name'         => __( 'Real Time Categories', 'fb' ),
		);

		$taxonomy_args = array(
			'hierarchical'      => true,
			'public'            => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'rewrite'           => array( 'slug' => 'realtime-category' ),
			'show_admin_column' => true,
		);

		$taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
