<?php
/**
 * Creative Revolution custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// CreativeRev post type
/*******************************************************************/

if ( ! class_exists('Flora_Fresh_Paint_Post_Type') ):

class Flora_Fresh_Paint_Post_Type {
	public static $post_type     = 'fb_freshpaint';
	// public static $taxonomy      = 'fb_freshpaint_category';
	public static $menu_position = 36;

	public static function init() {
		self::register();
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Fresh Paint', 'fb' ),
			'singular_name'      => __( 'Fresh Paint', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Page', 'fb' ),
			'edit_item'          => __( 'Edit Fresh Paint Page', 'fb' ),
			'new_item'           => __( 'New Fresh Paint Page', 'fb' ),
			'view_item'          => __( 'View Fresh Paint Page', 'fb' ),
			'search_items'       => __( 'Search Fresh Paint Pages', 'fb' ),
			'not_found'          => __( 'No fresh paint pages found', 'fb' ),
			'not_found_in_trash' => __( 'No fresh paint pages found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Fresh Paint', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'fresh-paint' ),
			'capability_type'    => 'page',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-desktop',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author', 'page-attributes' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		// $labels = array(
		// 	'name'              => __( 'Fresh Paint Categories', 'fb' ),
		// 	'singular_name'     => __( 'Fresh Paint Category', 'fb' ),
		// 	'search_items'      => __( 'Search in Category', 'fb' ),
		// 	'all_items'         => __( 'Fresh Paint Categories', 'fb' ),
		// 	'parent_item'       => __( 'Parent Fresh Paint Category', 'fb' ),
		// 	'parent_item_colon' => __( 'Parent Fresh Paint Category:', 'fb' ),
		// 	'edit_item'         => __( 'Edit Category', 'fb' ),
		// 	'update_item'       => __( 'Update Category', 'fb' ),
		// 	'add_new_item'      => __( 'Add New Fresh Paint Category', 'fb' ),
		// 	'new_item_name'     => __( 'New Category Name', 'fb' ),
		// 	'menu_name'         => __( 'Fresh Paint Categories', 'fb' )
		// );
		//
		// $taxonomy_args = array(
		// 	'hierarchical'      => true,
		// 	'public'            => true,
		// 	'labels'            => $labels,
		// 	'show_ui'           => true,
		// 	'rewrite'           => array( 'slug' => 'fresh-paint-category' ),
		// 	'show_admin_column' => true,
		// );

		// $taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		// register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
