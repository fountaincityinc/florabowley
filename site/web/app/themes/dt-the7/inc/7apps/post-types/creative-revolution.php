<?php
/**
 * Creative Revolution custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// CreativeRev post type
/*******************************************************************/

if ( ! class_exists('Flora_Creative_Revolution_Post_Type') ):

class Flora_Creative_Revolution_Post_Type {
	public static $post_type     = 'fb_creativerev';
	// public static $taxonomy      = 'fb_creative_revolution_category';
	public static $menu_position = 36;

	public static function init() {
		self::register();
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Creative Revolution', 'fb' ),
			'singular_name'      => __( 'Creative Revolution', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Page', 'fb' ),
			'edit_item'          => __( 'Edit Creative Revolution Page', 'fb' ),
			'new_item'           => __( 'New Creative Revolution Page', 'fb' ),
			'view_item'          => __( 'View Creative Revolution Page', 'fb' ),
			'search_items'       => __( 'Search Creative Revolution Pages', 'fb' ),
			'not_found'          => __( 'No creative revolution pages found', 'fb' ),
			'not_found_in_trash' => __( 'No creative revolution pages found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Creative Revolution', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'creative-revolution' ),
			'capability_type'    => 'page',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-desktop',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author', 'page-attributes' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		// $labels = array(
		// 	'name'              => __( 'Creative Revolution Categories', 'fb' ),
		// 	'singular_name'     => __( 'Creative Revolution Category', 'fb' ),
		// 	'search_items'      => __( 'Search in Category', 'fb' ),
		// 	'all_items'         => __( 'Creative Revolution Categories', 'fb' ),
		// 	'parent_item'       => __( 'Parent Creative Revolution Category', 'fb' ),
		// 	'parent_item_colon' => __( 'Parent Creative Revolution Category:', 'fb' ),
		// 	'edit_item'         => __( 'Edit Category', 'fb' ),
		// 	'update_item'       => __( 'Update Category', 'fb' ),
		// 	'add_new_item'      => __( 'Add New Creative Revolution Category', 'fb' ),
		// 	'new_item_name'     => __( 'New Category Name', 'fb' ),
		// 	'menu_name'         => __( 'Creative Revolution Categories', 'fb' )
		// );
		//
		// $taxonomy_args = array(
		// 	'hierarchical'      => true,
		// 	'public'            => true,
		// 	'labels'            => $labels,
		// 	'show_ui'           => true,
		// 	'rewrite'           => array( 'slug' => 'creative-revolution-category' ),
		// 	'show_admin_column' => true,
		// );

		// $taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		// register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
