<?php
/**
 * Collectives custom post type.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// Collectives post type
/*******************************************************************/

if ( ! class_exists('Flora_Collectives_Post_Type') ):

class Flora_Collectives_Post_Type {
	public static $post_type     = 'fb_collectives';
	public static $taxonomy      = 'fb_collectives_category';
	public static $menu_position = 37;

	public static function init() {
		self::register();
		add_image_size( 'collective-thumb', 640, 640, true );
	}

	public static function register() {
		// titles
		$labels = array(
			'name'               => __( 'Collectives', 'fb' ),
			'singular_name'      => __( 'Collectives', 'fb' ),
			'add_new'            => __( 'Add New', 'fb' ),
			'add_new_item'       => __( 'Add New Collective', 'fb' ),
			'edit_item'          => __( 'Edit Collective', 'fb' ),
			'new_item'           => __( 'New Collective', 'fb' ),
			'view_item'          => __( 'View Collective', 'fb' ),
			'search_items'       => __( 'Search Collectives', 'fb' ),
			'not_found'          => __( 'No collectives found', 'fb' ),
			'not_found_in_trash' => __( 'No collectives found in Trash', 'fb' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'The Collective', 'fb' ),
		);

		// options
		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'collectives' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => self::$menu_position,
			'menu_icon'          => 'dashicons-art',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions', 'author' )
		);

		$args = apply_filters( 'presscore_post_type_' . self::$post_type . '_args', $args );

		register_post_type( self::$post_type, $args );
		/* post type end */

		/* setup taxonomy */

		// titles
		$labels = array(
			'name'              => __( 'Collective Categories', 'fb' ),
			'singular_name'     => __( 'Collective Category', 'fb' ),
			'search_items'      => __( 'Search in Category', 'fb' ),
			'all_items'         => __( 'Collective Categories', 'fb' ),
			'parent_item'       => __( 'Parent Collective Category', 'fb' ),
			'parent_item_colon' => __( 'Parent Collective Category:', 'fb' ),
			'edit_item'         => __( 'Edit Category', 'fb' ),
			'update_item'       => __( 'Update Category', 'fb' ),
			'add_new_item'      => __( 'Add New Collective Category', 'fb' ),
			'new_item_name'     => __( 'New Category Name', 'fb' ),
			'menu_name'         => __( 'Collective Categories', 'fb' ),
		);

		$taxonomy_args = array(
			'hierarchical'      => true,
			'public'            => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'rewrite'           => array( 'slug' => 'collective-category' ),
			'show_admin_column' => true,
		);

		$taxonomy_args = apply_filters( 'presscore_taxonomy_' . self::$taxonomy . '_args', $taxonomy_args );

		register_taxonomy( self::$taxonomy, array( self::$post_type ), $taxonomy_args );
		/* taxonomy end */
	}
}

endif;
