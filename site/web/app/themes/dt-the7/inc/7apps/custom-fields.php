<?php
/**
 * 7apps custom fields.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

add_action( 'carbon_register_fields', 'sa_register_carbon_fields' );

function sa_register_carbon_fields() {

  Carbon_Container::factory('custom_fields', 'Help Options')
    ->show_on_post_type(array(
      'product', 'page', 'post',
      'fb_ecourses', 'fb_creativerev', 'fb_handson_cr', 'fb_heartson_cr', 'fb_botanicals',
      'fb_together_apart', 'fb_freshpaint', 'fb_reunite', 'fb_reunite_retreat', 'fb_golden_ticket',
      'fb_creative_wellness', 'fb_studio_diaries', 'fb_painting_sessions', 'fb_collectives',
      'fb_realtime'
    ))
    ->add_fields(array(

      Carbon_Field::factory('checkbox', 'fb_show_need_help', 'Show Need Help Button?')
        ->set_required(false)
        ->set_option_value('yes')
        ->help_text(''),

      Carbon_Field::factory('text', 'fb_need_help_link', 'Need Help Button Link')
        ->set_required(false)
        ->help_text('<strong>Defaults to:</strong> /support'),

    ));

}
