<?php
/**
 * Theme setup.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! function_exists( 'fb_setup_theme' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @since 1.0.0
	 */
	function fb_setup_theme() {
		add_filter( 'cron_schedules', 'add_custom_cron_schedules' );

		/**
		 * Custom menu for the Ecourse 'micro-site'
		 */
		register_nav_menus( array(
			'ecourse' => __( 'Ecourse Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Creative Revolution 'micro-site'
		 */
		register_nav_menus( array(
			'creativerev' => __( 'Creative Revolution Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Fresh Paint 'micro-site'
		 */
		register_nav_menus( array(
			'freshpaint' => __( 'Fresh Paint Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Hands On 'micro-site'
		 */
		register_nav_menus( array(
			'handson' => __( 'Hands On Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Hearts On 'micro-site'
		 */
		register_nav_menus( array(
			'heartson' => __( 'Hearts On Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Botanicals 'micro-site'
		 */
		register_nav_menus( array(
			'botanicals' => __( 'Botanicals Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the TogetherApart 'micro-site'
		 */
		register_nav_menus( array(
			'together-apart' => __( 'Together Apart Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Reunite 'micro-site'
		 */
		register_nav_menus( array(
			'reunite' => __( 'Reunite Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Reunite Retreat 'micro-site'
		 */
		register_nav_menus( array(
			'reunite-retreat' => __( 'Reunite Retreat Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Golden Ticket 'micro-site'
		 */
		register_nav_menus( array(
			'golden-ticket' => __( 'Golden Ticket Menu', 'fb' ),
		) );

		/**
		 * Custom menu for the Creative Wellness 'micro-site'
		 */
		register_nav_menus( array(
			'creative-wellness' => __( 'Creative Wellness Menu', 'fb' ),
		) );

		/**
		 * Load Carbon Fields Lib + Register Custom Fields
		 */
		require_once trailingslashit( get_template_directory() ) . '/lib/carbon-fields/carbon-fields.php';
		require_once trailingslashit( get_template_directory() ) . '/lib/timepicker/carbon-field-timepicker.php';
	}
	add_action( 'after_setup_theme', 'fb_setup_theme', 10 );

endif; // fb_setup_theme

if ( ! function_exists( 'add_custom_cron_schedules' ) ) :

	/**
	 * Add custom cron schedules
	 *
	 * @since 1.0.0
	 */
	function add_custom_cron_schedules( $schedules ) {
		$schedules['monthly'] = array(
			'interval'=> 2592000,
			'display'=> __( 'Once Every 30 Days' ),
		);
		return $schedules;
	}

endif; // add_custom_cron_schedules

if ( ! function_exists( 'sa_change_default_sidebar' ) ) :

	/**
	 * Change default sidebar position - Will only apply to new pages/posts,
	 * pages posts with already selected sidebar will not effect this.
	 */
	function sa_change_default_sidebar() {
		global $DT_META_BOXES;
		if ( $DT_META_BOXES ) {
			if ( isset($DT_META_BOXES[ 'dt_page_box-sidebar' ]) ) {
				$DT_META_BOXES[ 'dt_page_box-sidebar' ]['fields'][0]['std'] = 'disabled';
			}
		}
	}
	add_action( 'admin_init', 'sa_change_default_sidebar', 20 );

endif;

if ( ! function_exists( 'wpb_block_disqus' ) ) :

	/**
	 * removes Disqus from running on all the studio diaries
	 */
	function wpb_block_disqus($file) {
		if ( 'fb_studio_diaries' == get_post_type() ) {
			remove_filter('comments_template', 'dsq_comments_template');
		}
		return $file;
	}
	add_filter( 'comments_template' , 'wpb_block_disqus', 1 );

endif;
