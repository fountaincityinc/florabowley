<?php
/**
 * 7apps custom post types.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

require_once SEVEN_APPS_DIR . '/post-types/ecourse.php';
require_once SEVEN_APPS_DIR . '/post-types/creative-revolution.php';
require_once SEVEN_APPS_DIR . '/post-types/hands-on-cr.php';
require_once SEVEN_APPS_DIR . '/post-types/hearts-on-cr.php';
require_once SEVEN_APPS_DIR . '/post-types/botanicals.php';
require_once SEVEN_APPS_DIR . '/post-types/together-apart.php';
require_once SEVEN_APPS_DIR . '/post-types/reunite.php';
require_once SEVEN_APPS_DIR . '/post-types/reunite-retreat.php';
require_once SEVEN_APPS_DIR . '/post-types/golden-ticket.php';
require_once SEVEN_APPS_DIR . '/post-types/creative-wellness.php';
require_once SEVEN_APPS_DIR . '/post-types/fresh-paint.php';

require_once SEVEN_APPS_DIR . '/post-types/collectives.php';
require_once SEVEN_APPS_DIR . '/post-types/real-time.php';
require_once SEVEN_APPS_DIR . '/post-types/studio-diaries.php';
require_once SEVEN_APPS_DIR . '/post-types/painting-sessions.php';
// require_once SEVEN_APPS_DIR . '/post-types/workshops.php';
require_once SEVEN_APPS_DIR . '/post-types/product.php';

/////////////////////////
// Register post types //
/////////////////////////

if ( ! function_exists( 'fb_register_post_types' ) ) :

	function fb_register_post_types() {

		Flora_Ecourses_Post_Type::init();
    Flora_Creative_Revolution_Post_Type::init();
    Flora_HandsOn_PostType::init();
    Flora_HeartsOn_PostType::init();
    Flora_Botanicals_PostType::init();
    Flora_TogetherApart_PostType::init();
    Flora_Reunite_PostType::init();
    Flora_GoldenTicket_PostType::init();
    Flora_ReuniteRetreat_PostType::init();
    Flora_Fresh_Paint_Post_Type::init();
    Flora_CreativeWellness_PostType::init();

		Flora_Collectives_Post_Type::init();
		Flora_Real_Time_Post_Type::init();
		Flora_Studio_Diaries_Post_Type::init();
		Flora_Painting_Sessions_Post_Type::init();
		// Flora_Workshops_Post_Type::init();
		Flora_Product_Post_Type::init();

	}

endif;

add_action( 'init', 'fb_register_post_types', 10 );
