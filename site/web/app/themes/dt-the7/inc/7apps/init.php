<?php
/**
 * 7apps Init.
 *
 * @since 1.1.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Make sure to start a session for the ecommerce class
session_start();

// load constants.
require_once trailingslashit( get_template_directory() ) . 'inc/7apps/constants.php';

require_once SEVEN_APPS_DIR . '/helpers.php';
require_once SEVEN_APPS_DIR . '/static.php';
require_once SEVEN_APPS_DIR . '/custom-fields.php';
require_once SEVEN_APPS_DIR . '/post-types.php';
require_once SEVEN_APPS_DIR . '/ajax.php';
require_once SEVEN_APPS_DIR . '/theme-setup.php';
require_once SEVEN_APPS_DIR . '/classes/affiliates.php';
require_once SEVEN_APPS_DIR . '/classes/shopping-cart.php';
require_once SEVEN_APPS_DIR . '/classes/nav-menu/nav-menu.php';
require_once SEVEN_APPS_DIR . '/classes/video-player.php';
require_once SEVEN_APPS_DIR . '/classes/users.php';

if ( is_admin() ) {
	require_once SEVEN_APPS_DIR . '/admin/load-meta-boxes.php';
}

if ( class_exists( 'Woocommerce' ) ) {
	require_once SEVEN_APPS_DIR . '/woocommerce.php';
}



/* De-Registering small stylesheets from plugins to speed up the site */
add_action( 'wp_print_styles', 'flora_deregister_styles', 100 );

function flora_deregister_styles() {
	wp_deregister_style( 'wjecf-style' );
}


