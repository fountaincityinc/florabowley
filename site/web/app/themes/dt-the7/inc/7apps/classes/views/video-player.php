<!-- <h1> Studio Diary Video Player!!! </h1> -->
<!-- <div class="studio-diary-player<?php if ( $attr['text_below_video'] == 'yes' ) { echo ' text-below-video' ;} ?>"> -->
<div class="studio-diary-player text-below-video">
	<div class="studio-diary-player-video">
		<iframe
			src="https://player.vimeo.com/video/<?php echo $welcome_video_id; ?>"
			frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen
		></iframe>
	</div>
	<div class="studio-diary-player-video-details"
		<?php if ( !is_null( $attr['welcome_video_id'] ) ) { echo 'style="display: none;"'; } ?>
	>
		<?php if ( is_null( $attr['welcome_video_id'] ) ) : ?>
			<h4> <?php echo $videos[0]['fb_studio_diary_title']; ?> </h4>
			<?php echo apply_filters( 'the_content', $videos[0]['fb_studio_diary_desc'] ); ?>
		<?php endif; ?>
	</div>
	<div class="studio-diary-player-list row">
		<?php foreach( $videos as $k => $vid ) : ?>
		<div class="col-md-4 col-sm-6">
			<div class="studio-diary-player-list-item <?php if ( $k == 0 && is_null( $attr['welcome_video_id'] ) ) { echo 'active'; } ?>"
				data-video-id="<?php echo $vid['fb_studio_diary_vimeo_id']; ?>"
			>
				<div class="studio-diary-player-list-item-thumb">
					<i class="fa fa-<?php echo ( $k == 0 && is_null( $attr['welcome_video_id'] ) ) ? 'pause' : 'play'; ?>"></i>
					<?php $thumb_url = wp_get_attachment_image_src( $vid['fb_studio_diary_thumb'], 'medium' )[0]; ?>
					<img src="<?php echo $thumb_url; ?>" />
				</div>
				<div class="studio-diary-player-list-item-progress">
					<?php
						$percent  = 0;
						$meta_key = '_fb_studio_diary_video_'. $vid['fb_studio_diary_vimeo_id'] .'_progress';

						if ( is_user_logged_in() ) {
							$user_id = get_current_user_id();
							$percent = get_user_meta( $user_id, $meta_key, true );
						} else {
							$percent = $_SESSION[ $meta_key ];
						}
					?>
					<span class="progress" style="width: <?php echo round( floatval( $percent ) * 100, 3 ); ?>%;"></span>
				</div>
				<div class="studio-diary-player-list-item-content">
					<h4> <?php echo $vid['fb_studio_diary_title']; ?> </h4>
					<?php echo apply_filters( 'the_content', $vid['fb_studio_diary_desc'] ); ?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>
