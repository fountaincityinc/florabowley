<?php

// unset( $_SESSION['sa_cart_items'] );

class SA_Shopping_Cart {

	public static $items = null;

	public function __construct() {
		self::$items = new stdClass();

		if ( !empty( $_SESSION['sa_cart_items'] ) ) {
			self::$items = json_decode( $_SESSION['sa_cart_items'] );
		}

		add_shortcode( 'btn_register_workshop', array( $this, 'render_btn_register_workshop' ) );

		add_action( 'wp_ajax_sa_add_cart_item', array( $this, 'add_cart_item' ) );
		add_action( 'wp_ajax_nopriv_sa_add_cart_item', array( $this, 'add_cart_item' ) );

		add_action( 'wp_ajax_sa_remove_cart_item', array( $this, 'remove_cart_item' ) );
		add_action( 'wp_ajax_nopriv_sa_remove_cart_item', array( $this, 'remove_cart_item' ) );
	}

	public static function mini_cart() {
		self::render_mini_cart();
	}

	private static function render_mini_cart() {
		$cart_url = '';
		$pid = get_page_id_by_template_name( 'template-shopping-cart.php' );
		if ( $pid ) {
			$cart_url = get_permalink( $pid );
		}
		?>
			<div class="sa-mini-cart <?php if ( self::count_items() == 0 ) { print 'is-empty'; } ?>">
				<a class="" href="<?= $cart_url ?>">
					<span class="fa fa-shopping-cart"></span>
					Cart:
					<span class="sa-cart-total"> <?= self::get_formatted_total(); ?> </span>
					<span class="sa-cart-count"> <?= self::count_items(); ?> </span>
				</a>
			</div>
		<?php
	}

	public static function shopping_cart() {
		self::render_shopping_cart();
	}

	private static function render_shopping_cart() {
		?>
		<h3> <i class="fa fa-shopping-cart"></i> Items in your cart (<span class="sa-cart-count"> <?= count( self::$items ); ?> </span>) </h3>
		<div class="sa-shopping-cart">
			<?php foreach( self::$items as $i ) : ?>
			<div class="sa-cart-item" data-url="<?= $i->url; ?>">
				<div class="sa-cart-item-image" style="vertical-align: top; width: 150px;">
					<img src="<?= $i->image; ?>" height=100 />
				</div>
				<div class="sa-cart-item-details" style="vertical-align: top;">
					<h3> <?= $i->title; ?> </h3>
					<p> <?= $i->desc; ?> </p>
				</div>
				<div class="sa-cart-item-price" style="vertical-align: top; min-width: 90px;">
					<strong> <?= '$' . number_format( $i->price, 2 ); ?> </strong> <br>
				</div>

				<a href class="sa-remove-cart-item" data-item-id="<?= $i->id; ?>">
					<i class="fa fa-remove"></i>
				</a>
			</div>
			<?php endforeach; ?>
		</div>

		<div class="sa-cart-footer">
			<strong> Total </strong>
			<span class="sa-cart-total"> <?= self::get_formatted_total(); ?> </span>
		</div>

		<p style="text-align: right;">
			<button class="button"> Checkout </button>
		</p>

		<h3> Account Information </h3>
		<div class="sa-account-info">
		</div>
		<?php
	}

	public static function count_items() {
		return count( (array) self::$items );
	}

	public static function get_total() {
		$total = 0;
		foreach( self::$items as $i ) {
			$total += (float) $i->price;
		}
		return $total;
	}

	public static function get_formatted_total() {
		return '$' . number_format( self::get_total(), 2 );
	}

	private static function format_workshop_item( $post ) {
		return (object) array(
			'id'    => $post->ID,
			'title' => $post->post_title,
			'desc'  => $post->_fb_workshop_options_desc,
			'url'   => '/workshops', // get_the_permalink( $post->ID ),
			'image' => wp_get_attachment_image_src( $post->_fb_workshop_options_thumb[0], 'medium' )[0],
			'price' => $post->_fb_workshop_options_price,
		);
	}

	private static function format_ecourse_item( $post ) {
		return (object) array(
			'id'    => $post->ID,
			'title' => $post->post_title,
			'desc'  => '', // $post->_fb_workshop_options_desc,
			'url'   => '/ecourse', // get_the_permalink( $post->ID ),
			'image' => '', // wp_get_attachment_image_src( $post->_fb_workshop_options_thumb[0], 'medium' )[0],
			'price' => '', // $post->_fb_workshop_options_price,
		);
	}

	private static function format_studio_diary_item( $post ) {
		return (object) array(
			'id'    => $post->ID,
			'title' => $post->post_title,
			'desc'  => '', // $post->_fb_workshop_options_desc,
			'url'   => '/studio-diaries', // get_the_permalink( $post->ID ),
			'image' => '', // wp_get_attachment_image_src( $post->_fb_workshop_options_thumb[0], 'medium' )[0],
			'price' => '', // $post->_fb_workshop_options_price,
		);
	}

	public function add_cart_item() {
		$type = $_POST['type'];
		$id   = $_POST['id'];

		$post = get_post( $id );
		if ( $post->post_type == $type && !isset( self::$items->{ $id } ) ) {

			switch ( $type ) {
				case 'fb_workshops':
					$item = self::format_workshop_item( $post );
					break;
				case 'fb_ecourses':
					// $item = self::format_ecourse_item( $post );
					break;
				case 'fb_studio_diaries':
					// $item = self::format_studio_diary_item( $post );
					break;
			}
			self::$items->{ $id } = $item;
			$_SESSION['sa_cart_items'] = json_encode( self::$items );

			$output = array(
				'items' => self::$items,
				'count' => self::count_items(),
				'total' => self::get_formatted_total(),
			);
		} else {
			$output = array(
				'error' => 'Invalid Post',
			);
		}

		wp_send_json( $output );
	}

	public function remove_cart_item() {
		$item_id = $_POST['item_id'];
		if ( isset( self::$items->{ $item_id } ) ) {
			unset( self::$items->{ $item_id } );
			$_SESSION['sa_cart_items'] = json_encode( self::$items );

			$output = array(
				'items' => self::$items,
				'count' => self::count_items(),
				'total' => self::get_formatted_total(),
			);
		} else {
			$output = array(
				'error' => 'That item isn\'t in your cart',
				'items' => self::$items,
				'count' => self::count_items(),
				'total' => self::get_formatted_total(),
			);
		}

		wp_send_json( $output );
	}

	public function render_btn_register_workshop( $atts ) {
		$attr = shortcode_atts( array(
			'id'   => false,
			'text' => 'Register Now',

			// TODO: Allow user to custom the full text?
			// 'full_text' => 'Workshop Full'

			// TODO: Load an image to display above the button?
			// 'image' => '',
		), $atts );

		// TODO: Lookup fb_workshops post by ID and make sure it has room left
		// If it's full, change the $atts['text'] = 'Workshop Full'
		// and remove the .sa-add-cart-item class to prevent clicks
		$classes = array( 'sa-register-workshop' );
		if ( 1 /* Class Isn't Full */ ) {
			$classes[] = 'sa-add-cart-item';
		}

		// TODO: Could also use the post lookup to grab a featured image and
		// use as the image that appears above

		ob_start();
		?>
		<?php // get_the_post_thumbnail( $post->ID, 'full' ); ?>
		<h3 style="text-align: center;">
			<a href="#addCartItem"
				class="<?php echo join( ' ', $classes ); ?>"
				data-id="<?php echo $attr['id']; ?>"
				data-type="fb_workshops"
			>
				<?= $attr['text']; ?>
			</a>
		</h3>
		<?php
		return ob_get_clean();
	}

}

new SA_Shopping_Cart;
