<?php

class SA_Users {

	public function __construct() {
		add_shortcode( 'user_account_form', array( $this, 'render_account_form' ) );
		add_shortcode( 'user_change_password_form', array( $this, 'render_change_password_form' ) );

		add_action( 'wp_ajax_sa_user_account', array( $this, 'process_account_form' ) );
		add_action( 'wp_ajax_nopriv_sa_user_account', array( $this, 'process_account_form' ) );

		add_action( 'wp_ajax_sa_change_password', array( $this, 'process_change_password' ) );
		add_action( 'wp_ajax_nopriv_sa_change_password', array( $this, 'process_change_password' ) );
	}

	public function render_account_form( $atts ) {
		$attr = shortcode_atts( array(
			// 'id'   => false,
			// 'text' => 'Register Now',
		), $atts );

		$user = wp_get_current_user();

		ob_start();
		?>
		<form class="form-horizontal sa-form sa-user-form sa-user-account" method="post">
			<input type="hidden" name="action" value="sa_user_account" />
			<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('user_account_nonce'); ?>">
			<fieldset>

				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-7">
						<div class="alert alert-danger"> There was an error </div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">First Name</label>
					<div class="col-sm-7">
						<input type="text" name="firstname" value="<?php echo $user->user_firstname; ?>" class="form-control input-md" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Last Name</label>
					<div class="col-sm-7">
						<input type="text" name="lastname" value="<?php echo $user->user_lastname; ?>" class="form-control input-md" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Email</label>
					<div class="col-sm-7">
						<input type="text" name="email" value="<?php echo $user->user_email; ?>" class="form-control input-md" />
					</div>
				</div>

				<!-- <div class="form-group">
					<label class="col-sm-3 control-label">Username</label>
					<div class="col-sm-7">
						<input type="text" readonly="readonly" value="<?php echo $user->user_login; ?>" class="form-control input-md" />
					</div>
				</div> -->

				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-7">
						<input type="checkbox"
							id="optin"
							name="optin"
							value="true"
							style="margin-left: 24px; margin-top: 13px; margin-bottom: 24px; float: right;"
							<?php if ( $user->email_notifications == 'true' ) echo 'checked="checked"'; ?>
						/>
						<label class="control-label" style="text-align: right;" for="optin">Recieve email notifications</label>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-7">
						<button class="sa-save pull-right"> Save </button>
					</div>
				</div>
			</fieldset>
		</form>
		<?php
		return ob_get_clean();
	}

	public function process_account_form() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'user_account_nonce' ) ) {
			wp_send_json(array(
				'success' => false,
				'data'    => array( 'Security Issue' ),
			));
		}

		$user_id = wp_update_user( array(
			'ID'         => get_current_user_id(),
			'first_name' => $_POST['firstname'],
			'last_name'  => $_POST['lastname'],
			// 'user_login' => $_POST['username'],
			'user_email' => $_POST['email'],
		) );

		if ( is_wp_error( $user_id ) ) {
			// There was an error, probably that user doesn't exist.
			$output = array( 'success' => false, 'errors' => $user_id->get_error_messages() );
		} else {
			// Success!

			$optin = !empty( $_POST['optin'] ) ? 'true' : 'false';
			update_user_meta( $user_id, 'email_notifications', $optin );
			$output = array( 'success' => true );
		}

		wp_send_json( $output );
	}

	public function render_change_password_form( $atts ) {
		$attr = shortcode_atts( array(
			// 'id'   => false,
			// 'text' => 'Register Now',
		), $atts );

		ob_start();
		?>
		<form class="form-horizontal sa-form sa-user-form sa-user-change-password" method="post">
			<input type="hidden" name="action" value="sa_change_password" />
			<input type="hidden" name="nonce" value="<?php echo wp_create_nonce('user_password_nonce'); ?>">
			<fieldset>
				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-7">
						<div class="alert alert-danger"> There was an error </div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Password</label>
					<div class="col-sm-7">
						<input type="password" name="password" class="form-control input-md" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">Confirm</label>
					<div class="col-sm-7">
						<input type="password" name="confirm" class="form-control input-md" />
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-3"></div>
					<div class="col-sm-7">
						<button class="sa-save pull-right"> Save </button>
					</div>
				</div>
			</fieldset>
		</form>
		<?php
		return ob_get_clean();
	}

	public function process_change_password() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'user_password_nonce' ) ) {
			wp_send_json(array(
				'success' => false,
				'data'    => array( 'Security Issue' ),
			));
		}

		if ( empty( $_POST['password'] ) || empty( $_POST['confirm'] ) ) {
			wp_send_json(array(
				'success' => false,
				'errors'  => array( 'Missing required field.' ),
			));
		} else if ( $_POST['password'] !== $_POST['confirm'] ) {
			wp_send_json(array(
				'success' => false,
				'errors'  => array( 'Passwords do not match.' ),
			));
		}

		$user = wp_get_current_user();
		$new_pass = $_POST['password'];

		$user_id = wp_update_user( array(
			'ID'        => $user->ID,
			'user_pass' => $new_pass,
		) );

		if ( is_wp_error( $user_id ) ) {
			// There was an error, probably that user doesn't exist.
			$output = array(
				'success' => false,
				'errors'  => $user_id->get_error_messages(),
			);
		} else {
			$secure = ( $_SERVER['SERVER_PORT'] == 80 ) ? false : true;
			$login_data = array(
				'user_login'    => $user->user_login,
				'user_password' => $new_pass
			);
			$user_verify = wp_signon( $login_data, $secure );

			$output = array( 'success' => true );
		}

		wp_send_json( $output );
	}

}

new SA_Users;
