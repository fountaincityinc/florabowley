<?php
/**
 * FB Affiliates
 *
 * This class extends the AffiliateWP plugin by adding
 * a custom form (shortcode) to signup new affiliates with.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// FB Affiliates
/*******************************************************************/

if ( ! class_exists('Flora_Affiliates') ):

class Flora_Affiliates {

	public function __construct() {
		add_shortcode( 'affiliates_signup_form', __CLASS__ . '::render_signup_form' );

		// AJAX Methods
		add_action( 'wp_ajax_fb_aff_signup_form', __CLASS__ . '::process_signup_form' );
		add_action( 'wp_ajax_nopriv_fb_aff_signup_form', __CLASS__ . '::process_signup_form' );
	}

	public static function render_signup_form( $atts, $content = null ) {

		$attr = shortcode_atts( array(
			'welcome_video_id' => null,
			'text_below_video' => 'no',
		), $atts );

		// $videos = carbon_get_post_meta( get_the_ID(), 'fb_studio_diary_videos', 'complex' );

		// if ( !is_null( $attr['welcome_video_id'] ) ) {
		// 	$welcome_video_id = $attr['welcome_video_id'];
		// } else {
		// 	$welcome_video_id = $videos[0]['fb_studio_diary_vimeo_id'];
		// }

		ob_start();

		if ( is_user_logged_in() && self::is_affiliate( get_current_user_id() ) ) {
			require_once SEVEN_APPS_DIR . '/classes/views/affiliates-already-registered.php';
		} else {
			require_once SEVEN_APPS_DIR . '/classes/views/affiliates-signup-form.php';
		}

		return ob_get_clean();
	}

	public static function is_affiliate( $user_id ) {
		// global $wpdb;
		// $table_name = $wpdb->prefix . 'affiliate_wp_affiliates';
		// $row = $wpdb->get_row( "SELECT * FROM $table_name WHERE user_id = $user_id" );
		// return ( $row !== NULL ) ? true : false;
		return ( affwp_is_affiliate() && affwp_is_active_affiliate() );
	}

	public static function process_signup_form() {
		// if ( ! wp_verify_nonce( $_POST['nonce'], 'aff_signup_nonce' ) ) {
		// 	wp_send_json(array(
		// 		'success' => false,
		// 		'data'    => array( 'Security Issue' ),
		// 	));
		// }

		$error  = false;
		$output = array(
			'success' => true,
			'data'    => array(),
		);

		if ( empty( $_POST['user_id'] ) ) {
			// Create account
			$fname = $_POST['fname'];
			$lname = $_POST['lname'];
			$email = $_POST['email'];
			$uname = $_POST['username'];
			$pass  = $_POST['pass'];
			$conf  = $_POST['conf'];

			if ( strlen( $fname ) === 0 ) {
				$error = true;
				$output['data'][] = __( 'First name is required.', 'fb' );
			}

			if ( strlen( $lname ) === 0 ) {
				$error = true;
				$output['data'][] = __( 'Last name is required.', 'fb' );
			}

			if ( strlen( $email ) === 0 || !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$error = true;
				$output['data'][] = __( 'A valid email is required.', 'fb' );
			}

			if ( email_exists( $email ) ) {
				$error = true;
				$output['data'][] = __( 'That email is already registered.', 'fb' );
			}

			if ( strlen( $uname ) === 0 ) {
				$error = true;
				$output['data'][] = __( 'Username is required.', 'fb' );
			}

			if ( username_exists( $uname ) ) {
				$error = true;
				$output['data'][] = __( 'That username is already in use.', 'fb' );
			}

			if ( strlen( $pass ) === 0 ) {
				$error = true;
				$output['data'][] = __( 'Password is required.', 'fb' );
			}

			if ( $pass !== $conf ) {
				$error = true;
				$output['data'][] = __( 'Passwords don\'t match', 'fb' );
			}

			$url = $_POST['website'];
			if ( strlen( $url ) && !preg_match( '/^http/', $url ) ) {
				$url = 'http://' . $url;
			}

			$paypal = $_POST['paypal'];

			if ( strlen( $paypal ) === 0 || !filter_var( $paypal, FILTER_VALIDATE_EMAIL ) ) {
				$error = true;
				$output['data'][] = 'A valid payment email is required.';
			}

			if ( $error ) {
				$output['success'] = false;
				wp_send_json( $output );
			}

			$userdata = array(
				'user_login' => $uname,
				'user_email' => $email,
				'user_url'   => $url,
				'first_name' => $fname,
				'last_name'  => $lname,
				'user_pass'  => $pass,
				'role'       => 'customer', // add custom role here
				'show_admin_bar_front' => false,
			);
			$user_id = wp_insert_user( $userdata );

			if ( ! is_wp_error( $user_id ) && is_numeric( $user_id ) ) {
				// User created successfully

				// Attempt Login
				$secure = ( $_SERVER['SERVER_PORT'] == 80 ) ? false : true;
				$login_data = array(
					'user_login'    => $username,
					'user_password' => $password
				);
				$user_verify = wp_signon( $login_data, $secure );

				// User created externally
				// $user_obj = get_user_by( 'id', $user_id );
				// Send confirmation Email
				// send_activation_email( $user_obj );

				// TODO: send a welcome email?
			} else {
				$output['success'] = false;
				$output['data'] = array( $user_id->get_error_message() ); // 'Error with local database. Please try again.'
				wp_send_json( $output );
			}
		} else {
			$user_id = $_POST['user_id'];
			$output['user_id'] = $user_id;

			$url = $_POST['website'];
			if ( strlen( $url ) && !preg_match( '/^http/', $url ) ) {
				$url = 'http://' . $url;
			}

			$paypal = $_POST['paypal'];

			if ( strlen( $paypal ) === 0 || !filter_var( $paypal, FILTER_VALIDATE_EMAIL ) ) {
				$error = true;
				$output['data'][] = 'A valid payment email is required.';
			}

			if ( $error ) {
				$output['success'] = false;
				wp_send_json( $output );
			}
		}

		global $wpdb;
		$wpdb->insert( $wpdb->prefix . 'affiliate_wp_affiliates', array(
			'user_id'         => $user_id,
			'payment_email'   => $paypal,
			'status'          => 'active',
			'earnings'        => '0',
			'date_registered' => date( 'Y-m-d H:i:s' ),
		));

		$db_error = $wpdb->last_error;
		if ( $db_error ) {
			$error = true;
			$output['data'][] = $db_error;
		}

		if ( $error ) {
			$output['success'] = false;
		} else {
			$output['data'][] = 'Thank you for signing up for the affiliate program. Please check your email for information on getting started!';
			self::send_welcome_email( $user_id );
		}

		wp_send_json( $output );
	}

	public static function send_welcome_email( $user_id ) {
		$user = get_user_by( 'id', $user_id );

		$first = $user->first_name;
		$last  = $user->last_name;
		$full  = $first . ' ' . $last;
		$email = $user->user_email;

		$username = 'info@florabowley.com';
		$api_key  = '2f5175d8f98f715cc4b6ee054e80d8b0';
		$url      = 'https://api.madmimi.com/mailer';

		$body  = "---\n";
		$body .= 'first_name: '. ucwords( $first ) ."\n";
		$body .= 'last_name: '. ucwords( $last ) ."\n";
		$body .= 'full_name: '. ucwords( $full ) ."\n";
		$body .= 'email: '. $email ."\n";

		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_NOSIGNAL, 1 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 120 ); // 2 min

		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );

		$recipient = $full .' <'. urlencode( $email ) .'>';

		$promo_name = 'Welcome to the Affiliate Program!';

		$params = array(
			'username'       .'='. $username,
			'api_key'        .'='. $api_key,
			'promotion_name' .'='. $promo_name,
			'recipient'      .'='. $recipient,
			'subject'        .'='. $promo_name,
			// 'bcc'            .'='. 'ryan@7apps.com',
			'from'           .'='. 'Flora Bowley <no-reply@florabowley.com>',
			'body'           .'='. $body,
		);
		$param_str = join( '&', $params );

		curl_setopt( $ch, CURLOPT_POST, count( $params ) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $param_str );

		$data        = curl_exec( $ch );
		$curl_errno  = curl_errno( $ch );
		$curl_error  = curl_error( $ch );
		$status_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		curl_close( $ch );

		$promo_txt = 'Promotion: "'. $promo_name .'" sent to: '. $recipient;
		$log_msg = ( $status_code === 200 )
			? 'SUCCESS - '. $promo_txt .' with transaction id: '. $data
			: 'FAILURE - '. $promo_txt .' with status code: '. $status_code .' and msg: '. $data;
		sa_log( 'mad-mimi-send-log', $log_msg );

		$sps = get_user_meta( $user->ID, '_fb_sent_promos', true );
		$sent_promos = ( $sps !== false ) ? json_decode( $sps ) : array();

		if ( $status_code === 200 && $user ) {
			$sent_promos[] = $promo_name;
			$sent_promos = array_unique( $sent_promos );
			update_user_meta( $user->ID, '_fb_sent_promos', json_encode( $sent_promos ) );
		}
	}

}

endif;

new Flora_Affiliates;
