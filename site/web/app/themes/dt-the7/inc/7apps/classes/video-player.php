<?php
/**
 * FB Video Player class.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*******************************************************************/
// FB Video Player class
/*******************************************************************/

if ( ! class_exists('FB_Video_player') ):

class FB_Video_player {
	public static function init() {
		add_action( 'carbon_register_fields', __CLASS__ . '::register_carbon_fields' );

		add_shortcode( 'fb_video_player', __CLASS__ . '::render_fb_video_player' );
		// NOTE: Keep this older / deprecated version of the shortcode in here so the existing content doesn't break
		add_shortcode( 'studio_diary_video_player', __CLASS__ . '::render_fb_video_player' );

		// AJAX Methods
		add_action( 'wp_ajax_fb_update_video_progress', __CLASS__ . '::update_video_progress' );
		add_action( 'wp_ajax_nopriv_fb_update_video_progress', __CLASS__ . '::update_video_progress' );
	}

	public static function register_carbon_fields() {
		Carbon_Container::factory('custom_fields', 'Video Player Options')
			->show_on_post_type(array(
        'fb_studio_diaries', 'fb_painting_sessions', 'fb_collectives',
        'fb_realtime', 'fb_freshpaint',
        'fb_handson_cr', 'fb_heartson_cr', 'fb_botanicals', 'fb_together_apart',
        'fb_reunite', 'fb_reunite_retreat', 'fb_golden_ticket', 'fb_creative_wellness',
			))
			// ->show_on_template('templates/front-page.php')
			->add_fields(array(

				Carbon_Field::factory('complex', 'fb_studio_diary_videos', 'Videos')
					->add_fields(array(
						Carbon_Field::factory('text', 'fb_studio_diary_title', 'Title')
							->set_required(true)
							->help_text(''),
						Carbon_Field::factory('rich_text', 'fb_studio_diary_desc', 'Description')
							// ->set_required(true)
							->help_text(''),
						Carbon_Field::factory('attachment', 'fb_studio_diary_thumb', 'Thumbnail')
							->set_required(true)
							->help_text(''),
						Carbon_Field::factory('text', 'fb_studio_diary_vimeo_id', 'Vimeo ID')
							->set_required(true)
							->help_text(''),

						// -- Example Fields for Reference --
						// Carbon_Field::factory('attachment', 'crb_main_background', 'Background Image for the current hour.')
						// 	->set_required(true)
						// 	->help_text('Image that will be used as page background. It will be resized to full width and height'),
						// Carbon_Field::factory('radio', 'crb_body_class')
						// 	->set_required(true)
						// 	->help_text('Select which style of text should be displayed.')
						// 	->add_options(array('day' => 'Day', 'night' => 'Night')),
						// Carbon_Field::factory('time', 'crb_start_hour')
						// 	->set_required(true)
						// 	->help_text('Select the hour that image will be displayed.'),
						// Carbon_Field::factory('text', 'crb_button_text')
						// 	->help_text('Text that will be used by button in page first section'),
					)),

			));
	}

	public static function render_fb_video_player( $atts, $content = null ) {

		$attr = shortcode_atts( array(
			'welcome_video_id' => null,
			// 'text_below_video' => 'yes',
		), $atts );

		$videos = carbon_get_post_meta( get_the_ID(), 'fb_studio_diary_videos', 'complex' );

		if ( !is_null( $attr['welcome_video_id'] ) ) {
			$welcome_video_id = $attr['welcome_video_id'];
		} else {
			$welcome_video_id = $videos[0]['fb_studio_diary_vimeo_id'];
		}

		ob_start();
		require_once SEVEN_APPS_DIR . '/classes/views/video-player.php';
		return ob_get_clean();
	}

	public static function update_video_progress() {
		$user_id  = false;
		$video_id = intval( $_POST['video_id'] );
		$percent  = floatval( $_POST['percent'] );

		$meta_key = '_fb_studio_diary_video_'. $video_id .'_progress';

		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();
			update_user_meta( $user_id, $meta_key, $percent );
		} else {
			$_SESSION[ $meta_key ] = $percent;
		}

		$output = array(
			'success'  => true,
			'video_id' => $video_id,
			'percent'  => $percent,
			'user_id'  => $user_id,
		);

		wp_send_json( $output );
	}
}

FB_Video_player::init();

endif;
