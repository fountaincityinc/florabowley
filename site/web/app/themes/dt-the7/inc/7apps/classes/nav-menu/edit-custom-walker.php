<?php
/**
 * /!\ This is a copy of Walker_Nav_Menu_Edit class in core
 *
 * Create HTML list of nav menu input items.
 *
 * @package WordPress
 * @since 3.0.0
 * @uses Walker_Nav_Menu
 */

class Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu {

	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker_Nav_Menu::start_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker_Nav_Menu::end_lvl()
	 *
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {}

	/**
	 * Start the element output.
	 *
	 * @see Walker_Nav_Menu::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   Not used.
	 * @param int    $id     Not used.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $_wp_nav_menu_max_depth;
		$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

		ob_start();
		$item_id = esc_attr( $item->ID );
		$removed_args = array(
			'action',
			'customlink-tab',
			'edit-menu-item',
			'menu-item',
			'page-tab',
			'_wpnonce',
		);

		$original_title = '';
		if ( 'taxonomy' == $item->type ) {
			$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
			if ( is_wp_error( $original_title ) )
				$original_title = false;
		} elseif ( 'post_type' == $item->type ) {
			$original_object = get_post( $item->object_id );
			$original_title = get_the_title( $original_object->ID );
		}

		$classes = array(
			'menu-item menu-item-depth-' . $depth,
			'menu-item-' . esc_attr( $item->object ),
			'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
		);

		$title = $item->title;

		if ( ! empty( $item->_invalid ) ) {
			$classes[] = 'menu-item-invalid';
			/* translators: %s: title of menu item which is invalid */
			$title = sprintf( __( '%s (Invalid)' ), $item->title );
		} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
			$classes[] = 'pending';
			/* translators: %s: title of menu item in draft status */
			$title = sprintf( __('%s (Pending)'), $item->title );
		}

		$title = ( ! isset( $item->label ) || '' == $item->label ) ? $title : $item->label;

		$submenu_text = '';
		if ( 0 == $depth )
			$submenu_text = 'style="display: none;"';

		?>
		<li id="menu-item-<?php echo $item_id; ?>" class="<?php echo implode(' ', $classes ); ?>">
			<dl class="menu-item-bar">
				<dt class="menu-item-handle">
					<span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <span class="is-submenu" <?php echo $submenu_text; ?>><?php _e( 'sub item' ); ?></span></span>
					<span class="item-controls">
						<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
						<span class="item-order hide-if-js">
							<a href="<?php
								echo wp_nonce_url(
									add_query_arg(
										array(
											'action' => 'move-up-menu-item',
											'menu-item' => $item_id,
										),
										remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
									),
									'move-menu_item'
								);
							?>" class="item-move-up"><abbr title="<?php esc_attr_e('Move up'); ?>">&#8593;</abbr></a>
							|
							<a href="<?php
								echo wp_nonce_url(
									add_query_arg(
										array(
											'action' => 'move-down-menu-item',
											'menu-item' => $item_id,
										),
										remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
									),
									'move-menu_item'
								);
							?>" class="item-move-down"><abbr title="<?php esc_attr_e('Move down'); ?>">&#8595;</abbr></a>
						</span>
						<a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php esc_attr_e('Edit Menu Item'); ?>" href="<?php
							echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
						?>"><?php _e( 'Edit Menu Item' ); ?></a>
					</span>
				</dt>
			</dl>

			<div class="menu-item-settings" id="menu-item-settings-<?php echo $item_id; ?>">
				<?php if( 'custom' == $item->type ) : ?>
					<p class="field-url description description-wide">
						<label for="edit-menu-item-url-<?php echo $item_id; ?>">
							<?php _e( 'URL' ); ?><br />
							<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
						</label>
					</p>
				<?php endif; ?>
				<p class="description description-thin">
					<label for="edit-menu-item-title-<?php echo $item_id; ?>">
						<?php _e( 'Navigation Label' ); ?><br />
						<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
					</label>
				</p>
				<p class="description description-thin">
					<label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
						<?php _e( 'Title Attribute' ); ?><br />
						<input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
					</label>
				</p>
				<p class="field-link-target description">
					<label for="edit-menu-item-target-<?php echo $item_id; ?>">
						<input type="checkbox" id="edit-menu-item-target-<?php echo $item_id; ?>" value="_blank" name="menu-item-target[<?php echo $item_id; ?>]"<?php checked( $item->target, '_blank' ); ?> />
						<?php _e( 'Open link in a new window/tab' ); ?>
					</label>
				</p>
				<p class="field-css-classes description description-thin">
					<label for="edit-menu-item-classes-<?php echo $item_id; ?>">
						<?php _e( 'CSS Classes (optional)' ); ?><br />
						<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
					</label>
				</p>
				<p class="field-xfn description description-thin">
					<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
						<?php _e( 'Link Relationship (XFN)' ); ?><br />
						<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
					</label>
				</p>
				<p class="field-description description description-wide">
					<label for="edit-menu-item-description-<?php echo $item_id; ?>">
						<?php _e( 'Description' ); ?><br />
						<textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
						<span class="description"><?php _e('The description will be displayed in the menu if the current theme supports it.'); ?></span>
					</label>
				</p>

				<?php
				/* New fields insertion starts here */
				?>

				<!-- <pre class="cb-cf"><?php // var_dump($item); ?></pre> -->

				<?php if ( ! $item->menu_item_parent ) : ?>

					<p class="field-custom description description-thin">
						<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
							<?php _e( 'Icon' ); ?><br />
							<select id="edit-menu-item-fb_icon-<?php echo $item_id; ?>" class="widefat code edit-menu-item-fb_icon" name="menu-item-fb_icon[<?php echo $item_id; ?>]">
								<?php // echo esc_attr( $item->fb_icon ); ?>
								<option value=""> Select One </option>
								<option value="about" <?php if ( $item->fb_icon == 'about' ) { echo 'selected'; } ?>> About </option>
								<option value="community" <?php if ( $item->fb_icon == 'community' ) { echo 'selected'; } ?>> Community </option>
								<option value="ecourse" <?php if ( $item->fb_icon == 'ecourse' ) { echo 'selected'; } ?>> Ecourse </option>
								<option value="gallery" <?php if ( $item->fb_icon == 'gallery' ) { echo 'selected'; } ?>> Gallery </option>
								<option value="latest" <?php if ( $item->fb_icon == 'latest' ) { echo 'selected'; } ?>> Latest </option>
								<option value="shop" <?php if ( $item->fb_icon == 'shop' ) { echo 'selected'; } ?>> Shop </option>
								<option value="studio-diaries" <?php if ( $item->fb_icon == 'studio-diaries' ) { echo 'selected'; } ?>> Studio Diaries </option>
								<option value="workshops" <?php if ( $item->fb_icon == 'workshops' ) { echo 'selected'; } ?>> Workshops </option>
								<option value="image" <?php if ( $item->fb_icon == 'image' ) { echo 'selected'; } ?>> Custom Image </option>
							</select>
						</label>
					</p>

					<div class="field-custom description cb-thin <?php if ( $item->fb_icon !== 'image' ) { echo 'hidden'; } ?>">
						<label for="edit-menu-item-nav_image-<?php echo $item_id; ?>">
							<?php _e( 'Nav Image' ); ?>
							<small> (<?php print ( ! $item->menu_item_parent ) ? __( '85px Tall' ) : __( '300 x 200px' ); ?>) </small><br />

							<div class="cb-input">
								<div id="nav_image" class="cb-image-uploader cb-cf <?php if ( $item->nav_image ) { print 'has-value'; } ?>">
									<div class="cb-hidden">
										<input type="hidden" data-name="id"
											id="edit-menu-item-nav_image-<?php echo $item_id; ?>"
											name="menu-item-nav_image[<?php echo $item_id; ?>]"
											value="<?php echo esc_attr( $item->nav_image ); ?>"
										/>
									</div>
									<div class="view show-if-value cb-soh">
										<ul class="cb-hl cb-soh-target">
											<!-- <li><a class="cb-icon dark" data-name="edit" href="#"><i class="cb-sprite-edit"></i></a></li> -->
											<li><a class="cb-icon dark" data-name="remove" href="#"><i class="cb-sprite-delete"></i></a></li>
										</ul>
										<?php
											$nav_image_src = '';
											if ( $item->nav_image ) {
												$nav_image_src = wp_get_attachment_image_src( $item->nav_image, 'thumbnail' )[0];
											}
										?>
										<img data-name="image" src="<?php echo $nav_image_src ?>" alt="">
									</div>
									<div class="view hide-if-value">
										<p>
											<!-- No image selected -->
											<a data-name="add" class="cb-button" href="#">Add Image</a>
										</p>
									</div>
								</div>
							</div>

						</label>
					</div>

					<script type="text/javascript">
						(function($){
							$(document).ready(function() {
								$('select[id^="edit-menu-item-fb_icon-"]').on('change', function(e) {
									var $selectWrap = $(e.currentTarget).closest('.field-custom');
									if ( $(e.currentTarget).val() == 'image' ) {
										$selectWrap.next().removeClass('hidden');
									} else {
										$selectWrap.next().addClass('hidden');
									}
								});
							});
						})(jQuery);
					</script>

					<div class="clear"></div>

					<p class="field-custom description description-thin">
						<label for="edit-menu-item-text_color-<?php echo $item_id; ?>">
							<?php _e( 'Text Color' ); ?><br />
							<input type="text"
								id="edit-menu-item-text_color-<?php echo $item_id; ?>"
								class="widefat edit-menu-item-custom wp-color-picker"
								name="menu-item-text_color[<?php echo $item_id; ?>]"
								value="<?php echo esc_attr( $item->text_color ); ?>"
							/>
						</label>
					</p>

					<!-- <div class="clear"></div> -->

					<p class="field-custom description description-thin">
						<label for="edit-menu-item-slider_menu-<?php echo $item_id; ?>">
							<input type="checkbox"
								id="edit-menu-item-slider_menu-<?php echo $item_id; ?>"
								class="widefat edit-menu-item-custom"
								name="menu-item-slider_menu[<?php echo $item_id; ?>]"
								value="yes"
								<?php echo $item->slider_menu == 'yes' ? 'checked="checked"' : ''; ?>
							/>
							<?php _e( 'Enable Image Slider Menu' ); ?>
						</label>
					</p>

				<?php else : ?>

					<div class="field-custom description cb-thin">
						<label for="edit-menu-item-nav_image-<?php echo $item_id; ?>">
							<?php _e( 'Nav Image' ); ?>
							<small> (<?php print ( ! $item->menu_item_parent ) ? __( '85px Tall' ) : __( '300 x 200px' ); ?>) </small><br />

							<div class="cb-input">
								<div id="nav_image" class="cb-image-uploader cb-cf <?php if ( $item->nav_image ) { print 'has-value'; } ?>">
									<div class="cb-hidden">
										<input type="hidden" data-name="id"
											id="edit-menu-item-nav_image-<?php echo $item_id; ?>"
											name="menu-item-nav_image[<?php echo $item_id; ?>]"
											value="<?php echo esc_attr( $item->nav_image ); ?>"
										/>
									</div>
									<div class="view show-if-value cb-soh">
										<ul class="cb-hl cb-soh-target">
											<!-- <li><a class="cb-icon dark" data-name="edit" href="#"><i class="cb-sprite-edit"></i></a></li> -->
											<li><a class="cb-icon dark" data-name="remove" href="#"><i class="cb-sprite-delete"></i></a></li>
										</ul>
										<?php
											$nav_image_src = '';
											if ( $item->nav_image ) {
												$nav_image_src = wp_get_attachment_image_src( $item->nav_image, 'thumbnail' )[0];
											}
										?>
										<img data-name="image" src="<?php echo $nav_image_src ?>" alt="">
									</div>
									<div class="view hide-if-value">
										<p>
											<!-- No image selected -->
											<a data-name="add" class="cb-button" href="#">Add Image</a>
										</p>
									</div>
								</div>
							</div>

						</label>
					</div>

				<?php endif; ?>

				<?php
				/* New fields insertion ends here */
				?>

				<p class="field-move hide-if-no-js description description-wide">
					<label>
						<span><?php _e( 'Move' ); ?></span>
						<a href="#" class="menus-move-up"><?php _e( 'Up one' ); ?></a>
						<a href="#" class="menus-move-down"><?php _e( 'Down one' ); ?></a>
						<a href="#" class="menus-move-left"></a>
						<a href="#" class="menus-move-right"></a>
						<a href="#" class="menus-move-top"><?php _e( 'To the top' ); ?></a>
					</label>
				</p>

				<div class="menu-item-actions description-wide submitbox">
					<?php if( 'custom' != $item->type && $original_title !== false ) : ?>
						<p class="link-to-original">
							<?php printf( __('Original: %s'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
						</p>
					<?php endif; ?>
					<a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
					echo wp_nonce_url(
						add_query_arg(
							array(
								'action' => 'delete-menu-item',
								'menu-item' => $item_id,
							),
							admin_url( 'nav-menus.php' )
						),
						'delete-menu_item_' . $item_id
					); ?>"><?php _e( 'Remove' ); ?></a> <span class="meta-sep hide-if-no-js"> | </span> <a class="item-cancel submitcancel hide-if-no-js" id="cancel-<?php echo $item_id; ?>" href="<?php echo esc_url( add_query_arg( array( 'edit-menu-item' => $item_id, 'cancel' => time() ), admin_url( 'nav-menus.php' ) ) );
						?>#menu-item-settings-<?php echo $item_id; ?>"><?php _e('Cancel'); ?></a>
				</div>

				<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" />
				<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
				<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
				<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
				<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
				<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
			</div><!-- .menu-item-settings-->
			<ul class="menu-item-transport"></ul>
		<?php
		$output .= ob_get_clean();
	}

} // Walker_Nav_Menu_Edit
