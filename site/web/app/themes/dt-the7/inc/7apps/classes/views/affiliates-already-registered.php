<div class="woocommerce-info">
	You are already registered as an affiliate. <a href="/affiliate-area">Visit the Affiliate Area</a>.
</div>

<p>
	For reference, your referral URL is:
	<strong><?php echo apply_filters('the_content', '[affiliate_referral_url url="'. site_url('ecourse') .'"]'); ?></strong>
</p>
