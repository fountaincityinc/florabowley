<?php
	$user = false;
	if ( is_user_logged_in() ) {
		$user  = wp_get_current_user();
		$name  = get_user_meta( $user->ID, 'first_name', true );
		$email = $user->user_email;
	}
?>
<form class="affiliate-signup-form sa-form gform_wrapper" method="post">
	<input type="hidden" name="action" value="fb_aff_signup_form" />
	<input type="hidden" name="nonce" value="<?= wp_create_nonce('aff_signup_nonce'); ?>">

	<?php if ( $user ) : ?>

		<input type="hidden" name="user_id" value="<?php echo $user->ID; ?>">
		<div class="woocommerce-info">
			Hi <?php echo $name; ?>! (<?php echo $email; ?>)
			&nbsp;&nbsp; Not You? <a href="<?php echo wp_logout_url( get_the_permalink() ); ?>"> Click here to logout</a>
		</div>

	<?php else: ?>

		<div class="woocommerce-info">
			Already have an account? <a href="#" class="sa-profile-login">Please login first</a>
		</div>

	<?php endif; ?>

	<div class="alert alert-danger"></div>
  <div class="spinner"><i class="fa fa-spinner fa-spin"></i></div>

	<fieldset>

		<?php if ( !$user ) : ?>

			<div class="wpb_row wf-container">
				<div class="vc_col-sm-12 wpb_column">
					<label> Name <span class="gfield_required">*</span></label>
				</div>
			</div>

			<div class="wpb_row wf-container">
				<div class="vc_col-sm-6 wpb_column">
					<input type="text" name="fname" value="<?= $_POST['fname']; ?>" placeholder="First" required>
				</div>
				<div class="vc_col-sm-6 wpb_column">
					<input type="text" name="lname" value="<?= $_POST['lname']; ?>" placeholder="Last" required>
				</div>
			</div>

			<div class="wpb_row wf-container">
				<div class="vc_col-sm-12 wpb_column">
					<label> Account Email <span class="gfield_required">*</span></label>
					<input type="email" name="email" value="<?= $_POST['email']; ?>" placeholder="sample@domain.com" required>
				</div>
			</div>

			<div class="wpb_row wf-container">
				<div class="vc_col-sm-12 wpb_column">
					<label> Username <span class="gfield_required">*</span></label>
					<input type="text" name="username" value="<?= $_POST['username']; ?>" required>
				</div>
			</div>

			<div class="wpb_row wf-container">
				<div class="vc_col-sm-6 wpb_column">
					<label> Password <span class="gfield_required">*</span></label>
					<input type="password" name="pass" required>
				</div>
				<div class="vc_col-sm-6 wpb_column">
					<label> Confirm <span class="gfield_required">*</span></label>
					<input type="password" name="conf" required>
				</div>
			</div>

		<?php endif; ?>

		<div class="wpb_row wf-container">
			<div class="vc_col-sm-12 wpb_column">
				<label> PayPal Payment Email <span class="gfield_required">*</span></label>
				<input type="email" name="paypal" value="<?= $_POST['paypal']; ?>" placeholder="sample@domain.com" required>
			</div>
		</div>

		<div class="wpb_row wf-container">
			<div class="vc_col-sm-12 wpb_column">
				<label> Website </label>
				<input type="text" name="website" value="<?= $_POST['website']; ?>" placeholder="optional">
			</div>
		</div>

		<input type="submit" value="Sign Up!">

	</fieldset>
</form>
