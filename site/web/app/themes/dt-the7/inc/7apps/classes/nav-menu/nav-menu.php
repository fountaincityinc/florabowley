<?php

class NavMenu {

	/**
	 * Class contructor
	 **/
	public function __construct() {
		// Custom Menu Item Fields
		add_filter( 'wp_setup_nav_menu_item', array( $this, 'add_custom_nav_fields' ) );
		add_action( 'wp_update_nav_menu_item', array( $this, 'update_custom_nav_fields'), 10, 3 );
		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'edit_walker'), 10, 2 );

		add_action( 'admin_enqueue_scripts', array( $this, 'load_custom_wp_admin_style' ) );

		add_image_size( 'slider-menu-thumb', 600, 400 );
	}

	function load_custom_wp_admin_style() {
		global $pagenow;
		if ( $pagenow == 'nav-menus.php' ) {
			wp_enqueue_style( 'wp-color-picker' );

			wp_register_script( 'media-browser-js', SEVEN_APPS_URI . '/classes/nav-menu/media-browser.js', array( 'wp-color-picker' ), '1.0.0' );
			wp_enqueue_script( 'media-browser-js' );

			wp_register_style( 'media-browser-css', SEVEN_APPS_URI . '/classes/nav-menu/media-browser.css', NULL, '1.0.0' );
			wp_enqueue_style( 'media-browser-css' );

			wp_enqueue_media();
		}
	}

	/**
	 * Add Custom Fields to Menu Items
	 **/
	public function add_custom_nav_fields( $menu_item ) {
		$menu_item->slider_menu = get_post_meta( $menu_item->ID, '_menu_item_slider_menu', true );
		$menu_item->fb_icon     = get_post_meta( $menu_item->ID, '_menu_item_fb_icon', true );
		$menu_item->text_color  = get_post_meta( $menu_item->ID, '_menu_item_text_color', true );
		$menu_item->nav_image   = get_post_meta( $menu_item->ID, '_menu_item_nav_image', true );
		return $menu_item;
	}

	/**
	 * Update Custom Fields added to Menu Items
	 **/
	public function update_custom_nav_fields( $menu_item, $menu_item_db_id, $args ) {
		if ( is_array( $_REQUEST['menu-item-slider_menu'] ) ) {
			$slider_menu_value = $_REQUEST['menu-item-slider_menu'][ $menu_item_db_id ];
			update_post_meta( $menu_item_db_id, '_menu_item_slider_menu', $slider_menu_value );
		}

		if ( is_array( $_REQUEST['menu-item-fb_icon'] ) ) {
			$fb_icon_value = $_REQUEST['menu-item-fb_icon'][ $menu_item_db_id ];
			update_post_meta( $menu_item_db_id, '_menu_item_fb_icon', $fb_icon_value );
		}

		if ( is_array( $_REQUEST['menu-item-text_color'] ) ) {
			$text_color_value = $_REQUEST['menu-item-text_color'][ $menu_item_db_id ];
			update_post_meta( $menu_item_db_id, '_menu_item_text_color', $text_color_value );
		}

		if ( is_array( $_REQUEST['menu-item-nav_image'] ) ) {
			$nav_image_value = $_REQUEST['menu-item-nav_image'][ $menu_item_db_id ];
			update_post_meta( $menu_item_db_id, '_menu_item_nav_image', $nav_image_value );
		}
	}

	/**
	 * Define new Walker edit
	 **/
	public function edit_walker( $walker, $menu_id ) {
		return 'Walker_Nav_Menu_Edit_Custom';
	}

}

include 'edit-custom-walker.php';
include 'custom-walker.php';

new NavMenu;
