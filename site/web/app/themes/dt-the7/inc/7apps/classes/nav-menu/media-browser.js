(function( $, window, document, undefined ) {
	$( document ).on( 'ready', function() {

		$( '.wp-color-picker' ).wpColorPicker();

		var file_frame;
		var $addButton    = $( 'a[data-name=add]' );
		var $removeButton = $( 'a[data-name=remove]' );

		$addButton.on( 'click', function( e ) {
			e.preventDefault();

			var $parent = $( e.currentTarget ).parents( '.cb-image-uploader' ),
					$input  = $parent.find( '.cb-hidden input' ),
					$image  = $parent.find( 'img[data-name=image]' );

			// If the media frame already exists, reopen it.
			if ( file_frame ) {
				file_frame.open();
				return;
			}

			// Create the media frame.
			file_frame = wp.media.frames.file_frame = wp.media({
				title: 'Select Image',
				button: { text: 'Select' },
				multiple: false,
			});

			// When an image is selected, run a callback.
			file_frame.on( 'select', function() {
				// We set multiple to false so only get one image from the uploader
				attachment = file_frame.state().get( 'selection' ).first().toJSON();
				var src = '';
				if ( typeof attachment.sizes.thumbnail != 'undefined' ) {
					src = attachment.sizes.thumbnail.url;
				} else {
					src = attachment.sizes.full.url;
				}

				$parent.addClass( 'has-value' );
				$image.attr( 'src', src  );
				$input.val( attachment.id );
			});

			// Discard + reset frame var so that
			// reference to $parent updates properly
			file_frame.on( 'close', function() {
				setTimeout( function() {
					// detach
					file_frame.detach();
					file_frame.dispose();

					// reset var
					file_frame = null;
				}, 500);
			});

			// Finally, open the modal
			file_frame.open();
		});

		$removeButton.on( 'click', function( e ) {
			e.preventDefault();

			var $parent = $( e.currentTarget ).parents( '.cb-image-uploader' ),
					$input  = $parent.find( '.cb-hidden input' ),
					$image  = $parent.find( 'img[data-name=image]' );

			$image.attr( 'src', '' );
			$input.val( '' );
			$parent.removeClass( 'has-value' );
		});

	}); // end doc.ready
})( jQuery, window, document );
