<?php
/**
 * 7apps Helpers.
 *
 * @since 1.1.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

function clean_magic_quotes( $var ) {
	$var = get_magic_quotes_gpc() ? stripslashes( $var ) : $var;
	$var = mysql_real_escape_string( $var );
	return $var;
}

function get_page_id_by_template_name( $template_name ) {
	if ( $template_name == 'page.php' ) {
		$template_name = 'default';
	}

	$pages = get_posts(array(
		'post_type'      => 'page',
		'posts_per_page' => 1,
		'meta_key'       => '_wp_page_template',
		'meta_value'     => $template_name
	));

	if ( $pages ) {
		return $pages[0]->ID;
	}
	return false;
}

function sa_log( $file_name, $msg ) {
	$upload_dir = wp_upload_dir();
	$fb_uploads = $upload_dir['basedir'] . '/fb';

	$logs_dir = $fb_uploads . '/logs';
	if ( !is_dir( $logs_dir ) ) { wp_mkdir_p( $logs_dir ); }

	if ( !file_exists( $fb_uploads . '/.htaccess' ) ) {
		$hh = fopen( $fb_uploads . '/.htaccess', 'w' );
		fwrite( $hh, 'Deny from all' );
		fclose( $hh );
	}

	$log_file   = $logs_dir .'/'. $file_name .'-'. date( 'Ym' ) .'.txt';
	$log_handle = fopen( $log_file, 'a' );

	fwrite( $log_handle, '['. date( 'Y-m-d H:i:s' ) .'] '. $msg ."\n" );
	fclose( $log_handle );
}

function is_dev_team() {
	$dev_ips = [ '75.145.72.81', '73.37.41.105' ];
	if ( in_array( $_SERVER['REMOTE_ADDR'], $dev_ips ) ) {
		return true;
	}
	return false;
}
