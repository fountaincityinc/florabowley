<?php
/**
 * 7apps AJAX Methods.
 *
 * @since 1.1.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

add_action( 'wp_ajax_login', 'ajax_login' );
add_action( 'wp_ajax_nopriv_login', 'ajax_login' );
function ajax_login() {
	// if ( ! wp_verify_nonce( $_POST['nonce'], 'user_login_nonce' ) ) {
	// 	wp_send_json(array(
	// 		'type' => 'error',
	// 		'data' => array( 'Security Issue' )
	// 	));
	// }

	$result = array();

	///////////////
	// Get user attributes together
	$email    = $_POST['email'];
	$password = $_POST['password'];
	$redirect = $_POST['redirect_to'];

	// Error Checking
	// $email_pattern = '~^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$~';
	// $phone_pattern = '~^.*\s?\(?[0-9]{3}\)\s?[0-9]{3}\s?-\s?[0-9]{4}$~'; // +123 (000) 000-0000;

	$errors = array();

	if ( !$password ) {
		$errors[] = __( 'Please enter your password.', 'fb' );
	}

	if ( !$email ) {
		$errors[] = __( 'Please enter a valid Email Address.', 'fb' );
	}

	// Display errors (if any)
	if ( $errors ) {
		$result['type'] = 'error';
		$result['data'] = $errors;
		wp_send_json( $result );
	}

	// Log user in!
	global $wpdb;

	// Check if user is activated
	$type = is_email( $email ) ? 'email' : 'login';
	$check_user = get_user_by( $type, $email );
	if ( $check_user ) {
		// $activation_key = get_user_meta( $check_user->ID, 'activation_key', true );
		//
		// if ( gettype( $activation_key ) == 'string' && $activation_key !== '' ) {
		// 	$errors[] = __( 'Please click the activation link we sent to your email.', 'fb' );
		// 	$result['type'] = 'error';
		// 	$result['data'] = $errors;
		//
		// 	wp_send_json( $result );
		// }
	} else {
		$errors[] = __( 'Email or Password do not match.', 'fb' );

		// Display errors (if any)
		if ( $errors ) {
			$result['type'] = 'error';
			$result['data'] = $errors;
			wp_send_json( $result );
		}
	}

	// Attempt Login
	$secure = ( $_SERVER['SERVER_PORT'] == 80 ) ? false : true;
	$login_data = array(
		'user_login'    => $check_user->user_login,
		'user_password' => $password
	);
	$user_verify = wp_signon( $login_data, $secure );

	if ( is_wp_error( $user_verify ) ) {
		// Invalid Credentials
		$errors[] = __( 'Email or Password do not match.', 'fb' );

		// Display errors (if any)
		if ( $errors ) {
			$result['type'] = 'error';
			$result['data'] = $errors;
			wp_send_json( $result );
		}
	} else {
		// Valid Login
		$result['type'] = 'success';

		// Check for user saved redirect_url
		$user_redirect = get_user_meta( $user_verify->ID, 'redirect_url' );

		if ( isset( $user_redirect ) && $user_redirect != '' ) {
			$result['redirect'] = $user_redirect;

			// Remove redirect_url
			update_user_meta( $user_verify->ID, 'redirect_url', '' );
		} else {
			// Otherwise send to page they logged in from
			$result['redirect'] = $redirect;
		}

		wp_send_json( $result );
	}
}

add_action( 'wp_ajax_forgot', 'ajax_forgot' );
add_action( 'wp_ajax_nopriv_forgot', 'ajax_forgot' );
function ajax_forgot() {
	global $wpdb, $wp_hasher;

	// if ( ! wp_verify_nonce( $_POST['nonce'], 'user_forgot_nonce' ) ) {
	// 	wp_send_json(array(
	// 		'type' => 'error',
	// 		'data' => array( 'Security Issue' )
	// 	));
	// }

	$email    = $_POST['email'];
	$redirect = $_POST['redirect_to'];

	$result = array();
	$errors = array();

	if ( !$email ) {
		$errors[] = __( 'Please enter a valid Email Address.', 'fb' );
	} else if ( !$user = get_user_by( 'email', $email ) ) {
		$errors[] = __( 'Account not found.', 'fb' );
	}

	// Display errors (if any)
	if ( count( $errors ) > 0 ) {
		$result['type'] = 'error';
		$result['data'] = $errors;
		wp_send_json( $result );
	}

	$key = wp_generate_password( 20, false );

	do_action( 'retrieve_password_key', $user->user_login, $key );

	// Now insert the key, hashed, into the DB.
	if ( empty( $wp_hasher ) ) {
		require_once ABSPATH . 'wp-includes/class-phpass.php';
		$wp_hasher = new PasswordHash( 8, true );
	}

	$hashed = $wp_hasher->HashPassword( $key );

	$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

	// Send email notification
	WC()->mailer(); // load email classes
	do_action( 'woocommerce_reset_password_notification', $user->user_login, $key );

	$result['type'] = 'success';
	$result['data'] = array( __( 'Check your e-mail for the confirmation link.', 'fb' ) );
	wp_send_json( $result );
}

add_action( 'wp_ajax_register', 'ajax_register' );
add_action( 'wp_ajax_nopriv_register', 'ajax_register' );
function ajax_register() {
	// if ( ! wp_verify_nonce( $_POST['nonce'], 'user_register_nonce' ) ) {
	// 	wp_send_json(array(
	// 		'type' => 'error',
	// 		'data' => array( 'Security Issue' )
	// 	));
	// }

	// Get user attributes together
	$first_name = $_POST['fname'];
	$last_name  = $_POST['lname'];
	$email      = $_POST['email'];
	$username   = $_POST['username'];
	$password   = $_POST['password'];
	$confirm    = $_POST['password_confirm'];
	$optin      = $_POST['optin'];
	$redirect   = $_POST['redirect_to'];

	// Error Checking
	// $email_pattern = '~^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$~';
	// $phone_pattern = '~^.*\s?\(?[0-9]{3}\)\s?[0-9]{3}\s?-\s?[0-9]{4}$~'; // +123 (000) 000-0000;

	$errors = array();

	if ( ! $first_name ) {
		$errors[] = __( 'Please enter your First Name.', 'fb' );
	}

	if ( ! $last_name ) {
		$errors[] = __( 'Please enter your Last Name.', 'fb' );
	}

	if ( ! $email || ! is_email( $email ) ) {
		$errors[] = __( 'Please enter a valid Email Address.', 'fb' );
	}

	if ( ! $username ) {
		$errors[] = __( 'Please enter a Username.', 'fb' );
	}

	if ( username_exists( $username ) ) {
		$errors[] = __( 'That Username is already in use.', 'fb' );
	}

	if ( ! $password || ! $confirm || $password !== $confirm ) {
		$errors[] = __( 'Your passwords do not match.', 'fb' );
	}

	// Display errors (if any)
	if ( $errors ) {
		$result['type'] = 'error';
		$result['data'] = $errors;
		wp_send_json( $result );
	}

	$userdata = array(
		'user_login' => $username,
		'user_email' => $email,
		'first_name' => $first_name,
		'last_name'  => $last_name,
		'user_pass'  => $password,
		'role'       => 'subscriber', // add custom role here
		'show_admin_bar_front' => false,
	);
	$user_id = wp_insert_user( $userdata );

	if ( ! is_wp_error( $user_id ) && is_numeric( $user_id ) ) {
		// User created successfully

		// Attempt Login
		$secure = ( $_SERVER['SERVER_PORT'] == 80 ) ? false : true;
		$login_data = array(
			'user_login'    => $username,
			'user_password' => $password
		);
		$user_verify = wp_signon( $login_data, $secure );

		if ( isset( $optin ) && $optin == 'true' ) {
			update_user_meta( $user_id, 'email_notifications', 'true' );
		}

		// User created externally
		$user_obj = get_user_by( 'id', $user_id );

		// Send confirmation Email
		// send_activation_email( $user_obj );

		// TODO: send a welcome email? Can we use WC_Emails?

		$result['type']     = 'success';
		$result['redirect'] = $redirect;

		// Save rediret_url
		update_user_meta( $user_id, 'redirect_url', $redirect );

		// wp_redirect($_POST['redirect_to']); // Send them to previous page
	} else {
		$result['type'] = 'error';
		$result['data'] = array( $user_id->get_error_message() ); // 'Error with local database. Please try again.'
	}

	wp_send_json( $result );
}
