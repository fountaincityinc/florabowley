<?php
/**
 * Load Meta boxes
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

////////////////////
// Meta-Box class //
////////////////////

// !- Loaded by main theme
// require_once PRESSCORE_EXTENSIONS_DIR . '/meta-box.php';

//////////////////////
// Theme Meta Boxes //
//////////////////////

function sa_load_meta_boxes() {

	$metaboxes = array(
		'metaboxes-workshops',
		// 'metaboxes-studio-diaries',
		// 'metaboxes-courses',
	);

	foreach ( $metaboxes as $metabox ) {
		include_once locate_template( "inc/7apps/admin/meta-boxes/{$metabox}.php" );
	}

}
add_action( 'admin_init', 'sa_load_meta_boxes', 20 );
