;(function( $, _, window, document, undefined ){

	var Clipboard = function( $el ) {
		this.$el = $el;
		this.init();
	};

	Clipboard.prototype.init = function() {
		this.$btn   = this.$el.find( '.js-copy' );
		this.$field = this.$el.find( 'input' );
		this.$btn.on( 'click', this.handleClick.bind( this ) );
	};

	Clipboard.prototype.handleClick = function( e ) {
		e.preventDefault();

		var supported = document.queryCommandSupported( 'copy' );
		if ( supported ) {
			try {
				this.$field.select();
				document.execCommand( 'copy' );
			} catch ( e ) {
				supported = false;
			}
		}

		if ( !supported ) {
			// Fall back to an alternate (flash based) approach like ZeroClipboard
			// BOOO :(
			this.$btn.find( 'i' ).removeClass( 'fa-copy' ).append( '<span style="color: red;"> Unable to copy, please press Cmd+C to copy now </span>' );
			this.$field.seelct();

			window.setTimeout( this.resetBtn.bind( this ), 2000 );
		} else {
			this.$btn.hide();
			this.$btn.find( 'i' ).removeClass( 'fa-copy' ).addClass( 'fa-check-circle' );
			this.$btn.append( '<span> Copied </span>' ).fadeIn();

			window.setTimeout( this.resetBtn.bind( this ), 2000 );
		}
	};

	Clipboard.prototype.resetBtn = function() {
		var _this = this;

		this.$btn.fadeOut();

		window.setTimeout( function() {
			_this.$btn.find( 'i' ).removeClass( 'fa-check-cirle' ).addClass( 'fa-copy' );
			_this.$btn.find( 'span' ).remove();
			_this.$btn.fadeIn();
		}, 500);
	};

	$copyWraps = $( '.js-copy-wrap' );
	if ( $copyWraps.length > 0 ) {
		_.each( $copyWraps, function( wrap, i, copyWraps ) {
			wrap.clipboard = new Clipboard( $( wrap ) );
		});
	}

})( jQuery, _, window, document );
