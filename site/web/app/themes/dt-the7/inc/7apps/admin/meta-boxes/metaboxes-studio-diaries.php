<?php
/**
 * Workshops template and post meta boxes
 *
 * @package florabowley
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

global $DT_META_BOXES;

/***********************************************************/
// Location options
/***********************************************************/

$prefix = '_fb_studio_diaries_options_';

$DT_META_BOXES[] = array(
	'id'       => 'fb_page_box-studio_diaries_options',
	'title'    => _x( 'Studio Diary Options', 'backend metabox', 'fb' ),
	'pages'    => array( 'fb_studio_diaries' ),
	'context'  => 'normal',
	'priority' => 'core',
	'fields'   => array(

		// Type
		// array(
		// 	'name'    => _x('Type:', 'backend metabox', 'fb'),
		// 	'id'      => "{$prefix}type",
		// 	'type'    => 'radio',
		// 	'std'     => 'normal',
		// 	'options' => array(
		// 		'on-premise'  => _x('Bar/Restaurant', 'backend metabox', 'fb'),
		// 		'off-premise' => _x('Bottle Shop',    'backend metabox', 'fb'),
		// 	),
		// 	'divider'	=> 'bottom',
		// ),

		// Title
		array(
			'name'    => _x( 'Title:', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}title",
			'type'    => 'text',
			'std'     => '',
			'divider' => ''
		),

		// Desc
		array(
			'name'    => _x( 'Short Description:', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}desc",
			'type'    => 'textarea',
			'std'     => '',
			'divider' => 'top'
		),

		// Thumnail
		// array(
		// 	'name'             => _x( 'Thumbnail:<br><small style ="color: #999;">300 x 200</small>', 'backend metabox', 'fb' ),
		// 	'id'               => "{$prefix}thumb",
		// 	'type'             => 'image_advanced_mk2',
		// 	'max_file_uploads' => 1,
		// 	'divider'          => 'top'
		// ),

		// Vimeo Link
		array(
			'name'    => _x( 'Vimeo Link:', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}vimeo_link",
			'type'    => 'text',
			'std'     => '',
			'divider' => 'top'
		),

		// Class Limit
		// array(
		// 	'name'    => _x( 'Class Size/Limit:', 'backend metabox', 'fb' ),
		// 	'id'      => "{$prefix}limit",
		// 	'type'    => 'number',
		// 	'std'     => '15',
		// 	'divider' => 'top'
		// ),

		// Price
		// array(
		// 	'name'    => _x( 'Price:', 'backend metabox', 'fb' ),
		// 	'id'      => "{$prefix}price",
		// 	'type'    => 'number',
		// 	'std'     => '1250.00',
		// 	'divider' => 'top'
		// ),

	),
);
