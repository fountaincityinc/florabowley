;(function( $, _, hbs, window, document, undefined ){

	var ProductAttendeeList = function( $el ) {
		this.$el = $el;
		this.init();
	};

	ProductAttendeeList.prototype.init = function() {
		this.rowTmpl      = hbs.compile( $( '#product-attendee-list-row' ).html() );
		this.notFoundTmpl = hbs.compile( $( '#product-attendee-list-not-found' ).html() );

		this.$table = this.$el.find( '#product-attendee-list-table' );
		this.$body  = this.$table.find( 'tbody' );

		this.$el.find( 'select.product-list' ).on( 'change', this.handleChange.bind( this ) );

		hbs.registerHelper( 'formattedAddress', this.hbsHelperFormattedAddress );
		hbs.registerHelper( 'formattedPhone', this.hbsHelperFormattedPhone );
	};

	ProductAttendeeList.prototype.hbsHelperFormattedPhone = function( phone, options ) {
		if ( !phone || typeof phone == 'undefined' || phone === null ) {
			phone = '';
		}

		var str = phone.replace( /\D/g , '' );
		if ( str.length === 10 ) {
			return str.replace( /^(\d{3})(\d{3})(\d{4})/, '($1) $2-$3' );
		}
		return phone;
	};

	ProductAttendeeList.prototype.hbsHelperFormattedAddress = function( a, options ) {
		var str = '';

		str += a.address_1;
		if ( typeof a.address_2 !== 'undefined' && a.address_2 !== null && a.address_2.length > 0 ) {
			str += '<br>' + a.address_2;
		}
		str += '<br>' + a.city + ', ' + a.state + ' ' + a.postcode;

		return str;
	};

	ProductAttendeeList.prototype.handleChange = function( e ) {
		var that = this;

		e.preventDefault();

		this.$body.html( '' );

		var productId = $( e.currentTarget ).val();
		$.ajax({
			url: saGlobal.ajax_url,
			type: 'post',
			dataType: 'json',
			data: {
				action: 'fb_get_attendees_for_product',
				product_id: productId,
			},
			success: function( res ) {
				// console.log( res );

				if ( res.success ) {
					_.each( res.attendees, function( attendee, i, list ) {
						that.$body.append( that.rowTmpl( attendee ) );
					});
				} else {
					that.$body.append( that.notFoundTmpl() );
				}
			},
		});
	};

	$pal = $( '#product-attendee-list' );
	if ( $pal.length > 0 ) {
		$pal[0].productAttendeeList = new ProductAttendeeList( $pal );
	}

})( jQuery, _, Handlebars, window, document );
