<?php
/**
 * Workshops template and post meta boxes
 *
 * @package florabowley
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

global $DT_META_BOXES;

/***********************************************************/
// Location options
/***********************************************************/

$prefix = '_fb_workshop_options_';

$DT_META_BOXES[] = array(
	'id'       => 'fb_page_box-workshop_options',
	'title'    => _x( 'Workshop Options', 'backend metabox', 'fb' ),
	'pages'    => array( 'fb_workshops' ),
	'context'  => 'normal',
	'priority' => 'core',
	'fields'   => array(

		// Type
		// array(
		// 	'name'    => _x('Type:', 'backend metabox', 'fb'),
		// 	'id'      => "{$prefix}type",
		// 	'type'    => 'radio',
		// 	'std'     => 'normal',
		// 	'options' => array(
		// 		'on-premise'  => _x('Bar/Restaurant', 'backend metabox', 'fb'),
		// 		'off-premise' => _x('Bottle Shop',    'backend metabox', 'fb'),
		// 	),
		// 	'divider'	=> 'bottom',
		// ),

		// TimeStamp
		// array(
		// 	// container begin !!!
		// 	'before'  => '<div class="js-timestamp-editor" style="position: relative;">
		// 		<script id="js-timestamp-editor-tmpl" type="text/x-handlebars-template">
		// 			<div class="timestamp-wrap" style="display: none;">
		// 				<label>
		// 					<span class="screen-reader-text">Month</span>
		// 					<select class="mm">
		// 						<option value="01" data-text="Jan">01-Jan</option>
		// 						<option value="02" data-text="Feb">02-Feb</option>
		// 						<option value="03" data-text="Mar">03-Mar</option>
		// 						<option value="04" data-text="Apr">04-Apr</option>
		// 						<option value="05" data-text="May">05-May</option>
		// 						<option value="06" data-text="Jun">06-Jun</option>
		// 						<option value="07" data-text="Jul">07-Jul</option>
		// 						<option value="08" data-text="Aug">08-Aug</option>
		// 						<option value="09" data-text="Sep">09-Sep</option>
		// 						<option value="10" data-text="Oct">10-Oct</option>
		// 						<option value="11" data-text="Nov">11-Nov</option>
		// 						<option value="12" data-text="Dec">12-Dec</option>
		// 					</select>
		// 				</label> <label>
		// 					<span class="screen-reader-text">Day</span>
		// 					<input type="text" class="jj" value="" size="2" maxlength="2" autocomplete="off">
		// 				</label>, <label>
		// 					<span class="screen-reader-text">Year</span>
		// 					<input type="text" class="aa" value="" size="4" maxlength="4" autocomplete="off">
		// 				</label> @ <label>
		// 					<span class="screen-reader-text">Hour</span>
		// 					<input type="text" class="hh" value="" size="2" maxlength="2" autocomplete="off">
		// 				</label>:<label>
		// 					<span class="screen-reader-text">Minute</span>
		// 					<input type="text" class="mn" value="" size="2" maxlength="2" autocomplete="off">
		// 				</label>
		// 				<a href="#edit_timestamp" class="save-timestamp hide-if-no-js button">OK</a>
		// 				<a href="#edit_timestamp" class="cancel-timestamp hide-if-no-js button-cancel">Cancel</a>
		// 			</div>
		// 		</script>',
		//
		// 	'name'    => _x( 'Date/Time:', 'backend metabox', 'fb' ),
		// 	'id'      => "{$prefix}timestamp",
		// 	'type'    => 'text',
		// 	'std'     => date( 'M j, Y @ H:i' ),
		// 	'divider' => '',
		//
		// 	'after'   => '<a href="#edit" class="js-edit-timestamp" style="position: absolute; top: 0; right: 0;">Edit</a></div>',
		// ),

		// Desc
		array(
			'name'    => _x( 'Short Description:<br><small style="color: #999;">(for cart)</small>', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}desc",
			'type'    => 'textarea',
			'std'     => '',
			'divider' => ''
		),

		// Thumnail
		array(
			'name'             => _x( 'Thumbnail:<br><small style ="color: #999;">300 x 200</small>', 'backend metabox', 'fb' ),
			'id'               => "{$prefix}thumb",
			'type'             => 'image_advanced_mk2',
			'max_file_uploads' => 1,
			'divider'          => 'top'
		),

		// Date(s)
		array(
			'name'    => _x( 'Date(s):', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}date",
			'type'    => 'text',
			'std'     => '',
			'divider' => 'top'
		),

		// Class Limit
		array(
			'name'    => _x( 'Class Size/Limit:', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}limit",
			'type'    => 'number',
			'std'     => '15',
			'divider' => 'top'
		),

		// Price
		array(
			'name'    => _x( 'Price:', 'backend metabox', 'fb' ),
			'id'      => "{$prefix}price",
			'type'    => 'number',
			'std'     => '1250.00',
			'divider' => 'top'
		),

	),
);
