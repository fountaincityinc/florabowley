;(function( $, _, window, document, undefined ) {

	var TimestampEditor = function( $el ) {
		this.$el        = $el;
		this.$inputWrap = this.$el.find( '.rwmb-input' );
		this.$input     = this.$inputWrap.find( 'input' );
		this.fieldTmpl  = Handlebars.compile( $( '#js-timestamp-editor-tmpl' ).html() );
		this.$edit      = this.$el.find( '.js-edit-timestamp' );

		this.init();
	};

	TimestampEditor.prototype.init = function() {
		this.$input.attr( 'readonly', true );

		this.$inputWrap.append( this.fieldTmpl({}) );
		this.$timestamp = this.$inputWrap.find( '.timestamp-wrap' );
		this.$cancel    = this.$timestamp.find( '.cancel-timestamp' );
		this.$update    = this.$timestamp.find( '.save-timestamp' );

		this.$edit.on( 'click', this.show.bind( this ) );
		this.$cancel.on( 'click', this.hide.bind( this ) );
		this.$update.on( 'click', this.update.bind( this ) );
		this.$timestamp.find( 'input' ).on( 'keypress', this.preventSubmit.bind( this ) );

		this.time = moment( this.$input.val() );
	};

	TimestampEditor.prototype.show = function( e ) {
		if ( typeof e != 'undefined' ) { e.preventDefault(); }

		this.setTimestamp();

		this.$input.hide();
		this.$edit.hide();
		this.$timestamp.fadeIn();
	};

	TimestampEditor.prototype.hide = function( e ) {
		if ( typeof e != 'undefined' ) { e.preventDefault(); }

		this.$timestamp.hide();
		this.$input.fadeIn();
		this.$edit.fadeIn();
	};

	TimestampEditor.prototype.update = function( e ) {
		if ( typeof e != 'undefined' ) { e.preventDefault(); }

		this.setInput();
		this.hide();
	};

	TimestampEditor.prototype.setTimestamp = function() {
		this.$timestamp.find( '.mm' ).val( this.time.format( 'MM' ) ); // Month
		this.$timestamp.find( '.jj' ).val( this.time.format( 'DD' ) ); // Day
		this.$timestamp.find( '.aa' ).val( this.time.format( 'YYYY' ) ); // Year
		this.$timestamp.find( '.hh' ).val( this.time.format( 'HH' ) ); // Hour
		this.$timestamp.find( '.mn' ).val( this.time.format( 'mm' ) ); // Minute
	};

	TimestampEditor.prototype.setInput = function() {
		this.time.month( parseInt( this.$timestamp.find( '.mm' ).val() ) - 1 ); // Month
		this.time.date( this.$timestamp.find( '.jj' ).val() ); // Day
		this.time.year( this.$timestamp.find( '.aa' ).val() ); // Year
		this.time.hour( this.$timestamp.find( '.hh' ).val() ); // Hour
		this.time.minute( this.$timestamp.find( '.mn' ).val() ); // Minute

		this.$input.val( this.time.format('MMM DD, YYYY @ HH:mm') );
	};

	TimestampEditor.prototype.preventSubmit = function( e ) {
		// enter key pressed
		if ( e.keyCode == '13' ) {
			e.preventDefault();
			this.update();
			return false;
		}
	}

	$tsEditors = $( '.js-timestamp-editor' );
	if ( $tsEditors.length > 0 ) {
		_.each( $tsEditors, function( tse, i, tsEditors ) {
			tse.timestampEditor = new TimestampEditor( $( tse ) );
		});
	}

})( jQuery, _, window, document );
