<?php
/**
 * Constants.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! defined( 'SEVEN_APPS_DIR' ) ) {
	define( 'SEVEN_APPS_DIR', trailingslashit( PRESSCORE_DIR ) . '7apps' );
}

if ( ! defined( 'SEVEN_APPS_URI' ) ) {
	define( 'SEVEN_APPS_URI', trailingslashit( PRESSCORE_URI ) . '7apps' );
}

if ( ! defined( 'SEVEN_APPS_DEV' ) ) {
	define( 'SEVEN_APPS_DEV', stripos( $_SERVER['HTTP_HOST'], 'local.flora.com' ) > -1 );
}

if ( ! defined( 'SEVEN_APPS_IP' ) ) {
	$ip = ( $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ) ? '127.0.0.1' : '75.145.72.81';
	define( 'SEVEN_APPS_IP', $ip );
}
