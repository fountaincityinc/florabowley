<?php

/**
 * Woocommerce extensions / hooks.
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! function_exists( 'sa_schedule_promo_emails_for_order' ) ) :

	function sa_schedule_promo_emails_for_order( $order ) {
		// sa_log( 'mad-mimi-send-log', 'sa_schedule_promo_emails_for_order ID: ' . $order->id );

		$renewal = get_post_meta( $order->get_id(), '_subscription_renewal', true );
		$autogen = get_post_meta( $order->get_id(), '_sa_autogen_order', true );

		if ( $order->get_total() == 0 ) {
			add_action( 'woocommerce_email', 'unhook_completed_notification' );
		}

		if ( !$renewal && !$autogen ) {
			date_default_timezone_set('America/Los_Angeles');

			foreach( $order->get_items() as $line_item ) {
				$pid = $line_item['product_id'];

				$emails = carbon_get_post_meta( $pid, 'fb_product_emails', 'complex' );
				// sa_log( 'mad-mimi-send-log', 'found '. count( $emails ) .' emails for product ID: ' . $pid );
				foreach ( $emails as $email ) {

					if ( strlen($email['fb_product_email_delay']) === 0 || strtolower( $email['fb_product_email_delay'] ) === 'now' ) {
						// sa_log( 'mad-mimi-send-log', 'send email for product ID: ' . json_encode( $email ) . ' NOW!' );
						Flora_Product_Post_Type::send_promotion( $email, $order->get_id() );
					} else {
						// sa_log( 'mad-mimi-send-log', 'schedule email for product ID: ' . json_encode( $email ) );
						wp_schedule_single_event(
							strtotime( $email['fb_product_email_delay'] ),
							'fb_product_send_promotion',
							array( $email, $order->get_id() )
						);
					}
				}
			}
		}
	}

endif;

if ( ! function_exists( 'sa_complete_order_unless_physical_product' ) ) :

	function sa_complete_order_unless_physical_product( $order ) {
		$renewal = get_post_meta( $order->get_id(), '_subscription_renewal', true );
		$autogen = get_post_meta( $order->get_id(), '_sa_autogen_order', true );

		if ( !$renewal && !$autogen ) {
			if ( $order->has_status( 'completed' ) ) {
				sa_log( 'wc-orders', 'sa_complete_order_unless_physical_product: aborting - order already completed' );
				return;
			}

			$is_physical = false;
			foreach ( $order->get_items() as $line_item ) {
				$pid = $line_item['product_id'];

				// Check if it has category lifestyle-goodies or books
				$has_cat = has_term(['lifestyle-goodies', 'books'], 'product_cat', $pid);
				if ( !$is_physical && $has_cat === 'yes' ) {
					$is_physical = true;
				}
				sa_log( 'wc-orders', '[category] product '. $pid .' is in one of [lifestyle-goodies, books] ?? '. $has_cat );
			}

			if ( !$is_physical ) {
				sa_log( 'wc-orders', '[category] updating order '. $order->get_id() .' status to completed' );
				$order->update_status( 'completed' );
			} else {
				sa_schedule_promo_emails_for_order( $order );
			}
		}
	}

endif;

if ( ! function_exists( 'sa_wc_order_status_completed' ) ) :

	/**
	 * Check completed order and prevent email notification if total 0
	 */
	function sa_wc_order_status_completed( $order_id ) {
		$order = wc_get_order( $order_id );
		sa_log( 'wc-orders', '[completed] order_id: ' . $order_id . ' total: ' . $order->get_total() );

		sa_schedule_promo_emails_for_order( $order );
	}

	add_action( 'woocommerce_order_status_completed', 'sa_wc_order_status_completed' );

endif;

if ( ! function_exists( 'sa_wc_payment_complete' ) ) :

	/**
	 * Process order and trigger more stuff if it includes certain products
	 */
	function sa_wc_payment_complete( $order_id ) {
		$order = wc_get_order( $order_id );

		$user = get_user_by( 'id', $order->customer_user );
		if ( !$user && strlen( $order->billing_email ) ) {
			$user = get_user_by( 'email', $order->billing_email );
		}
		$user_id = $user->ID;

		sa_log( 'wc-orders', '[payment complete] order_id: ' . $order_id . ' user_id: ' . $user_id );

		// sa_complete_order_unless_physical_product( $order );
		sa_schedule_promo_emails_for_order( $order );
	}

	add_action( 'woocommerce_payment_complete', 'sa_wc_payment_complete', 9 );

endif;

if ( ! function_exists( 'sa_wc_create_order' ) ) :

	/**
	 * Create a new WC order
	 * + assign product + user
	 * + mark completed and set total 0
	 */
	function sa_wc_create_order( $product_id=false, $user_id=false, $address=array() ) {
		if ( !$product_id || !$user_id ) {
			sa_log( 'wc-orders', '[create] FAILURE - something went wrong, missing product or user id' );
			return;
		}

		$wc_emails = WC_Emails::instance();
		unhook_completed_notification( $wc_emails );

		$new_order = wc_create_order(array( 'customer_id' => $user_id ));

		update_post_meta( $new_order->id, '_sa_autogen_order', true );

		if ( count( $address ) ) {
			$new_order->set_address( $address, 'billing' );
			$new_order->set_address( $address, 'shipping' );
		}

		$new_order->add_product( get_product( $product_id ), 1, array( 'totals' => array( 'total' => 0 ) ) );
		$new_order->set_total( 0 );
		$new_order->update_status( 'completed' );

		sa_log( 'wc-orders', '[create] SUCCESS - order: ' . $order_id . ' product: ' . $product_id . ' user: ' . $user_id );
	}

endif;

if ( ! function_exists( 'unhook_completed_notification' ) ) :

	function unhook_completed_notification( $email_class ) {
		sa_log( 'wc-orders', '[unhook completed email]' );
		// Completed order emails
		remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );
	}

endif;


// changes the title of "my membership" section within My Account


function fb_members_content_section_title( $title ) {
    return __( 'My Online Courses', 'my-theme-text-domain' );
}
add_filter( 'wc_memberships_my_account_my_memberships_title', 'fb_members_content_section_title' );

function fb_members_content_section_interior_title( $title ) {
    return __( 'My Online Courses', 'my-theme-text-domain' );
}
add_filter( 'wc_memberships_members_area_my_membership_content_title', 'fb_members_content_section_title' );

add_filter ( 'woocommerce_account_menu_items', function( $menu_links ) {
  if ( $menu_links['members-area'] ) {
    $menu_links['members-area'] = 'My Courses';
  }

  // unset( $menu_links['edit-address'] ); // Addresses
  // unset( $menu_links['dashboard'] ); // Remove Dashboard
  // unset( $menu_links['payment-methods'] ); // Remove Payment Methods
  // unset( $menu_links['orders'] ); // Remove Orders
  // unset( $menu_links['members-area'] ); // Remove Memberships
  unset( $menu_links['scheduled-orders'] ); // Remove Scheduled Orders
  unset( $menu_links['downloads'] ); // Disable Downloads
  // unset( $menu_links['edit-account'] ); // Remove Account details tab
  // unset( $menu_links['customer-logout'] ); // Remove Logout link

  return $menu_links;
}, 999 );


// eliminates unnecessary columns for the Online Content Table under "My Account"
function sv_members_area_content_table_columns( $columns ) {
    if ( isset( $columns['membership-content-type'] ) ) {
        unset( $columns['membership-content-type'] );
    }
    if ( isset( $columns['membership-content-accessible'] ) ) {
        unset( $columns['membership-content-accessible'] );
    }

    return $columns;
}
add_filter('wc_memberships_members_area_my_membership_content_column_names', 'sv_members_area_content_table_columns', 10, 1 );

// eliminates unnecessary columns for the first screen of Online Content Table under "My Account"
function online_content_table_columns( $columns ) {
    if ( isset( $columns['membership-status'] ) ) {
        unset( $columns['membership-status'] );
    }
    if ( isset( $columns['membership-start-date'] ) ) {
        unset( $columns['membership-start-date'] );
    }
    if ( isset( $columns['membership-end-date'] ) ) {
        unset( $columns['membership-end-date'] );
    }
/* doesn't work
    if ( isset( $columns['membership-next-bill-on'] ) ) {
        unset( $columns['membership-next-bill-on'] );
    }
*/
    return $columns;
}
add_filter('wc_memberships_my_memberships_column_names', 'online_content_table_columns', 10, 1 );

/**
 * Remove the "Resubscribe" button from the My Subscriptions table.
 */
function fb_remove_my_subscriptions_button( $actions, $subscription ) {
	foreach ( $actions as $action_key => $action ) {
		switch ( $action_key ) {
			// case 'change_payment_method': // Hide "Change Payment Method" button?
			// case 'change_address':        // Hide "Change Address" button?
			// case 'switch':                // Hide "Switch Subscription" button?
			case 'resubscribe':           // Hide "Resubscribe" button from an expired or cancelled subscription?
			// case 'pay':                   // Hide "Pay" button on subscriptions that are "on-hold" as they require payment?
			// case 'reactivate':            // Hide "Reactive" button on subscriptions that are "on-hold"?
			// case 'cancel':                // Hide "Cancel" button on subscriptions that are "active" or "on-hold"?
				unset( $actions[ $action_key ] );
				break;
			default:
				error_log( '-- $action = ' . print_r( $action, true ) );
				break;
		}
	}
	return $actions;
}
add_filter( 'wcs_view_subscription_actions', 'fb_remove_my_subscriptions_button', 100, 2 );

function fb_add_facebook_pixel_purchase( $order_id ) {
	$order = wc_get_order( $order_id );
	// if ( $_SERVER['REMOTE_ADDR'] == '209.201.125.91' ) {
	// 	var_dump( $order );
	// }

	echo sprintf( "<script>
	if ('fbq' in window) {
		fbq('track', 'Purchase', { currency: '%s', value: %s });
	}
</script>", $order->currency, $order->total );
}
add_action( 'woocommerce_thankyou', 'fb_add_facebook_pixel_purchase', 10 );

// Replace WC Shortcode tag
add_filter( 'product_shortcode_tag', function ($shortcode) {
  return 'wc_orig_' . $shortcode;
});
add_shortcode( 'product', function ( $atts ) {
  if ( empty( $atts ) ) {
    return '';
  }

  $meta_query = WC()->query->get_meta_query();

  $args = array(
    'post_type'      => 'product',
    'posts_per_page' => 1,
    'no_found_rows'  => 1,
    'post_status'    => 'publish',
    'meta_query'     => $meta_query,
    'tax_query'      => WC()->query->get_tax_query(),
  );

  if ( isset( $atts['sku'] ) ) {
    $args['meta_query'][] = array(
      'key'     => '_sku',
      'value'   => $atts['sku'],
      'compare' => '=',
    );
  }

  if ( isset( $atts['id'] ) ) {
    $args['p'] = $atts['id'];
  }

  ob_start();

  $products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts, null ) );

  if ( $products->have_posts() ) : ?>

    <?php
      do_action( 'dt_wc_loop_start' );
      // woocommerce_product_loop_start();
    ?>

      <?php while ( $products->have_posts() ) : $products->the_post(); ?>

        <?php wc_get_template_part( 'content', 'product' ); ?>

      <?php endwhile; // end of the loop. ?>

    <?php
      // woocommerce_product_loop_end();
      do_action( 'dt_wc_loop_end' );
    ?>

  <?php endif;

  wp_reset_postdata();

  $css_class = 'woocommerce';

  if ( isset( $atts['class'] ) ) {
    $css_class .= ' ' . $atts['class'];
  }

  return '<div class="' . esc_attr( $css_class ) . '">' . ob_get_clean() . '</div>';
} );
