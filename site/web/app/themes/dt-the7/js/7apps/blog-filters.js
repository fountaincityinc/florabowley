;(function ($, window, document) {
  var BlogFilters = function ($links) {
    this.$links = $links;
    this.init();
  };

  BlogFilters.prototype.init = function () {
    if (window.location.hash.length > 0) {
      var hash = window.location.hash.replace(/^#/, '').replace(/\+/g, ' ');
      var links = this.$links.map(function filterLinks (i, link) {
        if ($(link).html().toLowerCase() === hash.toLowerCase()) { return link; }
      });
      if (links.length > 0) {
        links[0].click();
      }
    }
  };

  $(document).ready(function () {
    var $links = $('.filter-categories a');
    if ($links.length > 0) {
      window.blogFilters = new BlogFilters($links);
    }
  });
})(window.jQuery, window, document);
