;(function ($, window, document) {
  var ShoppingCart = function () {
    this.$miniCart = $('.sa-mini-cart');
    this.$shoppingCart = $('.sa-shopping-cart');

    this.init();
  };

  ShoppingCart.prototype.init = function () {
    this.setupEvents();
    return this;
  };

  ShoppingCart.prototype.setupEvents = function () {
    $('.sa-add-cart-item').on('click', this.addItem.bind(this));
    $('.sa-remove-cart-item').on('click', this.removeItem.bind(this));
    this.$shoppingCart.find('.sa-cart-item').on('click', this.goToItem);
  };

  ShoppingCart.prototype.updateCarts = function (count, total) {
    if (count > 0) {
      this.$miniCart.removeClass('is-empty');
    } else {
      this.$miniCart.addClass('is-empty');
    }
    $('.sa-cart-count').html(count);
    $('.sa-cart-total').html(total);
  };

  ShoppingCart.prototype.goToItem = function (e) {
    if (!$(e.target).is('.sa-remove-cart-item') && !$(e.target).is('.fa-remove')) {
      var url = $(e.currentTarget).data('url');
      window.location.href = url;
    }
  };

  ShoppingCart.prototype.addItem = function (e) {
    e.preventDefault();

    var $a = $(e.currentTarget);

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.saGlobal.ajax_url,
      data: {
        action: 'sa_add_cart_item',
        id: $a.data('id'),
        type: $a.data('type')
      },
      success: this.addItemSuccess.bind(this),
      error: this.addItemError.bind(this)
    });
  };

  ShoppingCart.prototype.addItemSuccess = function (res) {
    if (res.error) {
      // alert( res.error )
    } else {
      this.updateCarts(res.count, res.total);
    // alert('Your cart has been updated.')
    }
  };

  ShoppingCart.prototype.addItemError = function (xhr, ajaxOptions, thrownError) {
    console.log('error', thrownError);
  };

  ShoppingCart.prototype.removeItem = function (e) {
    var _this = this;
    e.preventDefault();

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.saGlobal.ajax_url,
      data: {
        action: 'sa_remove_cart_item',
        item_id: $(e.currentTarget).data('item-id')
      },
      success: function (res) {
        if (res.error) {
          window.alert(res.error);
        } else {
          $(e.currentTarget).closest('.sa-cart-item').slideUp();
          _this.updateCarts(res.count, res.total);
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log('error', thrownError);
      }
    });
  };

  $(document).ready(function () {
    window.shoppingCart = new ShoppingCart();
  });
})(window.jQuery, window, document);
