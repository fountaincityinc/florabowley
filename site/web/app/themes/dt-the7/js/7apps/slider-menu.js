;(function ($, window, document) {
  $(document).ready(function () {
    var $body = $('body');
    var $header = $('#header');
    var $adminBar = $('#wpadminbar');
    var $phantom = $('#phantom');
    var $sliderMenuItem = $('#main-nav li.has-slider-menu');
    var transitionEnd = window.transitionEndEventName();

    $sliderMenuItem.on('mouseenter', function (e) {
      var $menuItem = $(this);
      if (typeof this.sliderMenu === 'undefined') {
        this.sliderMenu = new SliderMenu($menuItem);
      }
      this.sliderMenu.phantom = ($menuItem.parents('#phantom').length === 1);
      // this.sliderMenu.setColor( $menuItem.find('.item-title').css( 'color' ) )
      this.sliderMenu.show();
    });

    $sliderMenuItem.on('mouseleave', function (e) {
      this.sliderMenu.hide();
    });

    var SliderMenu = function ($menuItem) {
      this.owl = null;
      this.$menuItem = $menuItem;
      this.menuItemId = $menuItem.data('id');
      this.$itemTitles = null;
      this.showing = false;
      this.phantom = false;
      this.closeTimeout = null;

      this.init();
    };

    SliderMenu.prototype.init = function () {
      this.$el = $('<div/>');
      this.$el.addClass('slider-menu');
      this.$el.attr('id', 'sm-' + this.menuItemId);

      var $subNavList = this.$menuItem.find('.sub-nav ul').clone();
      this.$el.append($subNavList);

      this.$itemTitles = this.$el.find('.item-title');

      this.setPosition();
      this.setupEvents();

      $body.append(this.$el);

      return this;
    }

    SliderMenu.prototype.setupEvents = function () {
      var _this = this;

      // this.$el.off( 'mousewheel' ).on( 'mousewheel', '.owl-stage', function( e ) {
      // 	e.preventDefault()
      // 	if ( e.deltaY < 0 ) {
      // 		_this.owl.trigger( 'next.owl.carousel' )
      // 	} else {
      // 		_this.owl.trigger( 'prev.owl.carousel' )
      // 	}
      // })

      this.$el.off('mouseenter').on('mouseenter', function () {
        window.clearTimeout(_this.closeTimeout);
        _this.closeTimeout = null;
      });

      this.$el.off('mouseleave').on('mouseleave', function () {
        _this.hide();
      });

      this.$el.find('li').off('mouseenter').on('mouseenter', function (e) {
        $(e.currentTarget).find('.item-title').css({ color: '' });
      });

      this.$el.find('li').off('mouseleave').on('mouseleave', function (e) {
        $(e.currentTarget).find('.item-title').css({ color: _this.color });
      });

      this.$el.off(transitionEnd).on(transitionEnd, function () {
        if (!_this.showing) {
          _this.$el.css({ zIndex: -1 });
        }
      });
    };

    // SliderMenu.prototype.setColor = function (color) {
    //   this.color = color
    //   this.darkenColor(-0.4)
    //   this.$itemTitles.css({ color: this.color })
    // }

    // SliderMenu.prototype.darkenColor = function (pct) {
    //   var color = this.color
    //   var f = color.split(',')
    //   var t = pct < 0 ? 0 : 255
    //   var p = pct < 0 ? pct * -1 : pct
    //   var R = parseInt(f[0].slice(4))
    //   var G = parseInt(f[1])
    //   var B = parseInt(f[2])
    //   this.color = 'rgb(' + (Math.round((t - R) * p) + R) + ',' + (Math.round((t - G) * p) + G) + ',' + (Math.round((t - B) * p) + B) + ')'
    // }

    SliderMenu.prototype.setPosition = function () {
      var o = 0;

      if (this.phantom === true) {
        this.$el.css({ position: 'fixed' });
        o = $phantom.outerHeight();
      } else {
        this.$el.css({ position: '' });
        o = $header.outerHeight();
      }

      if ($adminBar.length) {
        o += $adminBar.outerHeight();
      }

      this.$el.css({ top: o });

      return this;
    };

    SliderMenu.prototype.show = function () {
      var _this = this;

      if (this.closeTimeout) {
        window.clearTimeout(this.closeTimeout);
        this.closeTimeout = null;
      }

      this.$menuItem.addClass('slider-menu-in');

      this.$el.css({ zIndex: '' });
      this.setPosition();
      this.showing = true;

      if (!this.owl) {
        this.$el.on('initialized.owl.carousel', function () {
          _this.setupEvents();
          _this.$el.find('.slider-menu-prev, .slider-menu-next, .slider-menu-dots').show();
        });

        this.owl = this.$el.find('ul').owlCarousel({
          // itemElement: 'li',
          margin: 0,
          autoWidth: true,
          loop: true,
          center: true,
          nav: true,
          navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
          navClass: [ 'slider-menu-prev', 'slider-menu-next' ],
          dotsClass: 'slider-menu-dots',
          dotClass: 'slider-menu-dot'
        });
        this.setupEvents();
      }

      this.$el.addClass('in');
      if (this.phantom) {
        this.$el.addClass('phantom');
      } else {
        this.$el.removeClass('phantom');
      }
      return this;
    };

    SliderMenu.prototype.hide = function () {
      var _this = this;
      this.closeTimeout = window.setTimeout(function () {
        _this.showing = false;
        _this.$el.removeClass('in');
        _this.$menuItem.removeClass('slider-menu-in');
      }, 250);
      return this;
    };
  });
})(window.jQuery, window, document);
