;(function ($, window, document) {
  $(document).ready(function () {
    var $hashLinks = $('a[href^="#"]')
    var $phantom = $('#phantom')
    var $adminBar = $('#wpadminbar')
    $hashLinks.on('click', function (e) {
      e.preventDefault()

      var $link = $(e.currentTarget)
      var hash = $link.attr('href').replace('#', '')
      var $section = $('.section-' + hash)

      if ($section.length) {
        var _top =
          $section.offset().top -
          $phantom.outerHeight() -
          $adminBar.outerHeight()
        $('html, body').stop().animate({ scrollTop: _top }, 1e3)
      }
    })
  })
})(window.jQuery, window, document)
