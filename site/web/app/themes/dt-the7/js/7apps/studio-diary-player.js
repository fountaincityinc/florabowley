; (function ($, _, window, document) {
  var StudioDiaryPlayer = function ($player) {
    this.$player = $player;
    this.iframe = this.$player.find('iframe')[0];
    this.player = null;
    this.playing = false;
    this.$details = this.$player.find('.studio-diary-player-video-details');
    this.$items = this.$player.find('.studio-diary-player-list-item');
    this.$active = null;
    this.activeId = null;
    this.init();
  };

  StudioDiaryPlayer.prototype.init = function () {
    var _this = this;

    this.$items.on('click', this.handleClick.bind(this));

    this.initPlayer();

    window.onbeforeunload = function handleBeforeUnload() {
      if (_this.playing) {
        return 'A video is still playing and your progress has not been saved.';
      } else {
        // Make sure a confirmation box doesn't appear
        return null;
      }
    };
  };

  StudioDiaryPlayer.prototype.initPlayer = function () {
    var _this = this;

    this.player = new Vimeo.Player(this.iframe);
    this.player.on('play', this.onPlay.bind(this));
    this.player.on('pause', this.onPause.bind(this));
    this.player.on('ended', this.onEnded.bind(this));
    this.player.on('progress', this.onProgress.bind(this));
    // this.player.on('seeked', this.onSeeked.bind(this));

    this.player.ready().then(function () {
      // console.log( 'vimeo player ready' )

      if (_this.$active !== null) {
        window.setTimeout(function () {
          _this.playVideo();
        }, 500);
      }
    });
  };

  StudioDiaryPlayer.prototype.handleClick = function (e) {
    var _this = this;

    e.preventDefault();

    if (this.$active === null || this.activeId !== $(e.currentTarget).data('video-id')) {
      this.$active = $(e.currentTarget);
      this.activeId = this.$active.data('video-id');
      this.changeVideo();
    } else if (this.activeId === $(e.currentTarget).data('video-id')) {
      this.player.getPaused().then(function (paused) {
        if (paused === false) {
          _this.pauseVideo();
        } else {
          _this.playVideo();
        }
      });
    }
  };

  StudioDiaryPlayer.prototype.pauseVideo = function () {
    this.player.pause();
    this.playing = false;

    if (this.$active === null) { return; }

    // this.$active
    //   .addClass('active')
    //   .find('.fa').removeClass('fa-pause').addClass('fa-play');
  };

  StudioDiaryPlayer.prototype.playVideo = function () {
    if (this.$active === null) { return; }

    // this.$active
    //   .addClass('active')
    //   .find('.fa').removeClass('fa-play').addClass('fa-pause');

    /**
     * Disable SeekTo and Play
     *

    var _this = this;

    var width = /width: (\d{1,3}?.?\d{0,3})%;/.exec(this.$active.find('.progress').attr('style'));
    var pct = parseFloat(width[1]) / 100;
    if (pct > 0.95) { pct = 0; }
    var seconds = 0;
    this.player.getDuration().then(function (duration) {
      seconds = duration * pct;
      _this.player.setCurrentTime(seconds);
      _this.player.play();
      _this.playing = true;
    });

    */

    // NOTE: Use this for now instead:
    this.player.play();
    this.playing = true;
  };

  StudioDiaryPlayer.prototype.changeVideo = function () {
    if (this.activeId === null) { return; }

    var _this = this;

    this.player.loadVideo(this.activeId)
      .then(function () {
        _this.$items
          .removeClass('active')
          .find('.fa').removeClass('fa-pause').addClass('fa-play');

        var details = _this.$active.find('.studio-diary-player-list-item-content').html();
        _this.$details.html(details).fadeIn();

        _this.scrollUp();

        window.setTimeout(function () {
          _this.playVideo();
        }, 500);
      });
  };

  StudioDiaryPlayer.prototype.scrollUp = function () {
    var _offset = this.$player.offset().top;
    _offset -= $('#phantom').outerHeight();
    if ($('#wpadminbar').length) {
      _offset -= $('#wpadminbar').outerHeight();
    }
    _offset -= 20;
    $('html,body').animate({ scrollTop: _offset }, 500);
  };

  StudioDiaryPlayer.prototype.onProgress = function (data, id) {
    if (this.$active === null) { return; }

    this.$active.find('.progress').css({ width: (data.percent * 100) + '%' });
    // this.updateUserProgress(this.$active.data('video-id'), data.percent);
  };

  StudioDiaryPlayer.prototype.onEnded = function () {
    this.playing = false;
    this.updateUserProgress(this.$active.data('video-id'), 0);
  };

  // StudioDiaryPlayer.prototype.onSeeked = function (data) {
  //   this.updateUserProgress(this.$active.data('video-id'), data.percent);
  // };

  StudioDiaryPlayer.prototype.onPause = function (data) {
    if (this.$active === null) { return; }

    // this.updateUserProgress(this.$active.data('video-id'), data.percent);

    this.$active
      .addClass('active')
      .find('.fa').removeClass('fa-pause').addClass('fa-play');
  };

  StudioDiaryPlayer.prototype.onPlay = function (data) {
    if (this.$active === null) { return; }

    // this.updateUserProgress(this.$active.data('video-id'), data.percent);

    this.$active
      .addClass('active')
      .find('.fa').removeClass('fa-play').addClass('fa-pause');
  };

  StudioDiaryPlayer.prototype.updateUserProgress = _.debounce(function (videoId, percent) {
    $.ajax({
      url: window.saGlobal.ajax_url,
      type: 'post',
      data: {
        action: 'fb_update_video_progress',
        percent: percent,
        video_id: videoId
      },
      // success: function (res) { console.log( res ); }
    });
  }, 500);

  $(document).ready(function () {
    var $player = $('.studio-diary-player');
    if ($player.length > 0) {
      _.each($player, function (player, i, players) {
        player.userForm = new StudioDiaryPlayer($(player));
      });
    }
  });
})(window.jQuery, window._, window, document);
