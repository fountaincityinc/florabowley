;(function ($, window, document) {
  var LoginModal = function ($modal) {
    this.$modal = $modal;
    this.init();
  };

  LoginModal.prototype.init = function () {
    this.$modal.find('form').on('submit', this.submitForm);
    this.$modal.find('.toggle-tab').on('click', this.switchTab.bind(this));
    // this.$modal.find('.forgot').on('click', this.showForgot.bind(this));
    this.$modal.find('.remember').on('click', this.showLogin.bind(this));
  };

  LoginModal.prototype.showForgot = function (e) {
    e.preventDefault();
    this.$modal.find('.login-form').hide();
    this.$modal.find('.forgot-form').show();
  };

  LoginModal.prototype.showLogin = function (e) {
    e.preventDefault();
    this.$modal.find('.login-form').show();
    this.$modal.find('.forgot-form').hide();
  };

  LoginModal.prototype.switchTab = function (e) {
    e.preventDefault();
    var target = $(e.currentTarget).attr('href');
    this.$modal.find('.nav-tabs a[href=' + target + ']').click();
  };

  LoginModal.prototype.submitForm = function (e) {
    e.preventDefault();

    var $form = $(e.currentTarget);

    $form.find('fieldset').hide();
    $form.find('.spinner').fadeIn();
    $form.find('.alert').removeClass('alert-success').addClass('alert-danger').empty().hide();

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.saGlobal.ajax_url,
      data: $form.serialize(),
      success: function (response) {
        if (response.type === 'success') {
          if (response.redirect) {
            window.location.replace(response.redirect);
          } else {
            $form.find('.alert')
              .removeClass('alert-danger').addClass('alert-success')
              .append('<p>' + response.data[0] + '</p>');

            $form.find('.spinner').fadeOut(function () {
              $form.find('fieldset').show();
              $form.find('.alert').slideDown();
            });
          }
        } else {
          // Display Errors
          for (var i = 0; i < response.data.length; i++) {
            $form.find('.alert').append('<p>' + response.data[i] + '</p>');
          }

          $form.find('.spinner').fadeOut(function () {
            $form.find('fieldset').show();
            $form.find('.alert').slideDown();
          });
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $form.find('.alert').empty(); // Clean old errors
        $form.find('.alert').append('<p>Error: ' + thrownError + '</p>');
      }
    });
  };

  $(document).ready(function () {
    var $login = $('.sa-profile-login');

    if ($login.length > 0) {
      var $modal = $('#sign-in-modal');

      $login.on('click', function (e) {
        e.preventDefault();
        $modal.modal();
      });

      window.loginModal = new LoginModal($modal);

      if (window.location.hash === '#login') {
        $login.click();
      }
    }
  });
})(window.jQuery, window, document);
