;(function ($, window, document) {
  var AffiliateSignupForm = function ($form) {
    this.$form = $form;
    this.init();
  };

  AffiliateSignupForm.prototype.init = function () {
    this.$form.on('submit', this.submitForm);
  };

  AffiliateSignupForm.prototype.submitForm = function (e) {
    e.preventDefault();

    var $form = $(e.currentTarget);
    var $reqFields = $form.find('[required]');

    $reqFields.removeClass('error');

    $reqFields.each(function (i, input) {
      var $input = $(input);
      if ($input.val().length === 0) {
        $input.addClass('error');
      }
    });

    if ($reqFields.filter('.error').length) {
      return;
    }

    $form.find('fieldset').hide();
    $form.find('.spinner').fadeIn();
    $form.find('.alert').removeClass('alert-success').addClass('alert-danger').empty().hide();

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.saGlobal.ajax_url,
      data: $form.serialize(),
      success: function (res) {
        if (res.success === true) {
          $form.find(':input').not('[type=submit]').val('');

          $form.find('.alert')
            .removeClass('alert-danger').addClass('alert-success')
            .append('<p>' + res.data[0] + '</p>');

          $form.find('.spinner').fadeOut(function () {
            $form.find('fieldset').show();
            $form.find('.alert').slideDown();
          });
        } else {
          // Display Errors
          for (var i = 0; i < res.data.length; i++) {
            $form.find('.alert').append('<p>' + res.data[i] + '</p>');
          }

          $form.find('.spinner').fadeOut(function () {
            $form.find('fieldset').show();
            $form.find('.alert').slideDown();
          });
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $form.find('.alert').empty(); // Clean old errors
        $form.find('.alert').append('<p>Error: ' + thrownError + '</p>');
      }
    });
  };

  $(document).ready(function () {
    var $form = $('.affiliate-signup-form');

    if ($form.length > 0) {
      $.each($form, function (i, form) {
        form.signupForm = new AffiliateSignupForm($(form));
      });
    }

    // var $affWpLinkGen = $('#affwp-affiliate-dashboard-url-generator');
    // if ($affWpLinkGen.length) {
    //   var h4Str = $affWpLinkGen.find('h4').text().replace(' Generator', '');
    //   $affWpLinkGen.find('h4').text(h4Str);

    //   // 1. Hide the Link Generator
    //   $affWpLinkGen.find('p').eq(1).hide();
    //   $affWpLinkGen.find('form').hide();

    //   // 2. Clone the affiliate URL twice
    //   $affWpLinkGen.find('p').eq(0).after( $affWpLinkGen.find('p').eq(1).clone() );
    //   $affWpLinkGen.find('p').eq(0).after( $affWpLinkGen.find('p').eq(1).clone() );

    //   // 3. Modify the 3 URLs to these custom URLs
    //   var customURLs = [ '/ecourse', '/online-learning/studio-diaries', '/creative-revolution-ecourse' ];
    //   jQuery.each(customURLs, function(i, url) {
    //     var urlTxt = $affWpLinkGen.find('p').eq(i + 1).find('strong').text();
    //     var urlStr = urlTxt.replace(window.saGlobal.site_url + '/?ref', window.saGlobal.site_url + url + '/?ref');
    //     $affWpLinkGen.find('p').eq(i + 1).find('strong').text(urlStr);
    //   });
    // }
  });
})(window.jQuery, window, document);
