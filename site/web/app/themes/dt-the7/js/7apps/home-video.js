;(function($, window, document) {
  var HomeVideo = function($elem) {
    this.$elem = $elem
    this.init()
  }

  HomeVideo.prototype.init = function() {
    this.$elem.addClass('mfp-ready').magnificPopup({
      type: 'iframe',
      items: [
        {
          src: '//player.vimeo.com/video/118733576?autoplay=1'
        }
      ],
      iframe: {
        markup:
          '<div class="mfp-iframe-scaler">' +
          '<div class="mfp-close"></div>' +
          '<iframe src="" class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
          '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button

        // Templating object key. First part defines CSS selector.
        // Second attribute "iframe_src" means: find "iframe" and set attribute "src".
        srcAction: 'iframe_src'
      }
    })
  }

  $(document).ready(function() {
    var $header = $('.home #fancy-header')
    if ($header.length > 0) {
      $header.on('click', function() {
        window.location = '/news-art-love/'
      })
    }

    var $homeVideo = $('.home-video')
    if ($homeVideo.length > 0) {
      $homeVideo.each(function(i, elem) {
        elem.homeVideo = new HomeVideo($(elem))
      })
    }
  })
})(window.jQuery, window, document)
