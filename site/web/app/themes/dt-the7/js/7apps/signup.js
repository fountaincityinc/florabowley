;(function($, window, document) {
  var Signup = function($signup) {
    this.$signup = $signup
    this.$form = $signup.find('form')
    this.$submitButton = $signup.find('input[type=submit]')
    this.$icon = $signup.find('.sa-signup-open .fa')
    this.iconName = this.$icon[0].classList.value
      .split(' ')
      .filter(function(c) {
        return c !== 'fa'
      })
      .join(' ')
    this.init()
  }

  Signup.prototype.init = function() {
    this.$signup.find('.sa-signup-open').on('click', this.showSignup.bind(this))
    this.$signup.find('.field').on('focusout', this.hideSignup.bind(this))
    this.$form.on('submit', this.submit.bind(this))
  }

  Signup.prototype.showSignup = function(e) {
    e.preventDefault()
    this.$signup.addClass('active')
    this.$signup.find('.field').focus()
  }

  Signup.prototype.hideSignup = function(e) {
    this.$signup.removeClass('active')
  }

  Signup.prototype.submit = function(e) {
    var _this = this

    e.preventDefault()

    // this.$signup
    //   .find('.form-error')
    //   .html('')
    //   .hide()

    this.$submitButton.attr('disabled', 'disabled').addClass('disabled')
    this.$icon.removeClass(this.iconName).addClass('fa-spin fa-spinner')

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.mmns.ajax_url,
      data: this.$form.serialize(),
      success: function(res) {
        _this.$submitButton.removeAttr('disabled').removeClass('disabled')
        _this.$icon.removeClass('fa-spin fa-spinner').addClass(_this.iconName)

        console.log(res)
        if (res.error) {
          // _this.$signup
          //   .find('.form-error')
          //   .html(res.message)
          //   .show()
        } else {
          window.location = res.redirect
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        // $form.find( '.alert' ).empty(); // Clean old errors
        // $form.find( '.alert' ).append( '<p>Error: ' + thrownError + '</p>' );
      }
    })
  }

  $(document).ready(function() {
    var $signup = $('.sa-signup')
    if ($signup.length > 0) {
      $signup.each(function(i, el) {
        el.signup = new Signup($(el))
      })
    }
  })
})(window.jQuery, window, document)
