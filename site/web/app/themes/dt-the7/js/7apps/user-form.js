;(function ($, _, window, document) {
  var UserForm = function ($form) {
    this.$form = $form;
    this.$save = this.$form.find('.sa-save');
    this.$alert = this.$form.find('.alert');
    this.init();
  };

  UserForm.prototype.init = function () {
    this.$form.on('submit', this.submitForm.bind(this));
  };

  UserForm.prototype.submitForm = function (e) {
    var _this = this;
    e.preventDefault();

    this.$save.attr('disabled', true).html('Processing');
    this.$alert.empty().hide();

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: window.saGlobal.ajax_url,
      data: this.$form.serialize(),
      success: function (res) {
        if (res.success) {
          window.setTimeout(function () {
            _this.$save.attr('disabled', false).html('Saved!');
          }, 1000);
        } else {
          window.setTimeout(function () {
            _this.$save.attr('disabled', false).html('Error!');
          }, 1000);

          // Display Errors
          _.each(res.errors, function (err, i, errors) {
            _this.$alert.append('<p>' + err + '</p>');
          });
          _this.$alert.slideDown();
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        // $form.find( '.alert' ).empty(); // Clean old errors
        // $form.find( '.alert' ).append( '<p>Error: ' + thrownError + '</p>' );
      }
    });
  };

  $(document).ready(function () {
    var $forms = $('.sa-user-form');
    if ($forms.length > 0) {
      _.each($forms, function (form, i, forms) {
        form.userForm = new UserForm($(form));
      });
    }
  });
})(window.jQuery, window._, window, document);
