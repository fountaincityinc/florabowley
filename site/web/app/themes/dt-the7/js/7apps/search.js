;(function ($, window, document) {
  var Search = function ($search) {
    this.$search = $search;
    this.init();
  };

  Search.prototype.init = function () {
    this.$search.find('.sa-search-open').on('click', this.showSearch.bind(this));
    this.$search.find('.field').on('focusout', this.hideSearch.bind(this));
  };

  Search.prototype.showSearch = function (e) {
    e.preventDefault();
    this.$search.addClass('active');
    this.$search.find('.field').focus();
  };

  Search.prototype.hideSearch = function (e) {
    this.$search.removeClass('active');
  };

  $(document).ready(function () {
    var $search = $('.sa-search');
    if ($search.length > 0) {
      window.search = new Search($search);
    }
  });
})(window.jQuery, window, document);
