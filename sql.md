# Useful SQL Queries

Find Memberships (wc_user_membership) for Plan (post_parent)

```sql
select * from fb2_posts where post_type = 'wc_user_membership' and post_parent = '121400'
```

Find Membership (wc_user_membership) Expiration Dates (\_end_date) for Plan (post_parent)

```sql
select * from fb2_postmeta where post_id in (
	select ID from fb2_posts where post_type = 'wc_user_membership' and post_parent = '121400'
) and meta_key = '_end_date';
```

Change Membership (wc_user_membership) Expiration Dates (\_end_date) for Plan (post_parent)

```sql
update fb2_postmeta set meta_value = '2020-01-01 07:00:00' where post_id in (
	select ID from fb2_posts where post_type = 'wc_user_membership' and post_parent = '121400'
) and meta_key = '_end_date'
```

Activate (wcm-active) Expired (wcm-expired) Memberships (wc_user_membership)

```sql
update fb2_posts set post_status = 'wcm-active' where post_type = 'wc_user_membership' and post_parent = '121400' and post_status = 'wcm-expired'
```
