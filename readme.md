## Setup

This project uses [Roots.io](https://roots.io/) and [Ansible](https://www.ansible.com/) playbooks to run all the deployment and provisioning scripts.

Be sure to install the requirements described in [Installing Trellis](https://roots.io/trellis/docs/installing-trellis/) and [Remote Server Setup](https://roots.io/trellis/docs/remote-server-setup/). Depending on when you are reading this, you may encounter issues with newer versions of ansible. The project is currently known to work with 2.7, so be sure to install that version instead, by running:

```
pip install ansible==2.7
```

Python virtual environment(s) can be helpful for managing the installed ansible version and/or upgrading to new ones. Ansible provides details about this under the "Using pip" section of their [Installing Ansible Guide](https://ansible-tips-and-tricks.readthedocs.io/en/latest/ansible/install/). This project is setup to automatically ignore virtual environment(s) files, so long as they are stored in `trellis/.virtualenv`. Here is a modified example of what Ansible has in their docs, made to work in this environment:

```
pip install --upgrade pip virtualenv virtualenvwrapper
cd trellis
virtualenv .virtualenv/ansible2.7
source .virtualenv/ansible2.7/bin/activate
pip install ansible==2.7
```

If you are using [oh-my-zsh](https://ohmyz.sh/) you may run into issues with the `$PATH` not getting properly updated when activating the virtualenv, so you may want to add the following snippet to your `~/.zshrc`:

```bash
if [ -z "$VIRTUAL_ENV" ]; then
else
    export PATH="$VIRTUAL_ENV/bin:$PATH"
fi
```

This project uses [Composer](https://getcomposer.org), be sure to visit their website and follow instructions to install it as well.

## Staging

To visit staging site, go to: https://staging.florabowley.com/

## Development

To start a server locally for testing / dev, run:

```
vagrant up
```

Then you should be able to open up [florabowley.local](http://florabowley.local) in your browser. If this is your first time setting up the project though, you'll want to download all the uploads and import a copy of the database from the production/staging site.

### database access

Setup sequel pro (or another database GUI app) based on the "Development (Vagrant box)" settings described in [Database Access](https://roots.io/trellis/docs/database-access/).

The username is based on the domain name found in `trellis/group_vars/development/wordpress_sites.yml`, replacing the period with an underscore. In this case, it should be `florabowley_com` and the database name should be `florabowley_com_development`.

The password can be found in the `trellis/group_vars/development/vault.yml` which requires `ansible-vault` to read. Look for the `db_password:` key after running the following command from the trellis directory:

```bash
ansible-vault edit group_vars/development/vault.yml
```

> This opens the file with VIM, so when you're done, type `:q` and hit enter to exit the editor.

The first time you connect to the vagrant database with sequel pro, you may be presented with a popup to confirm the authenticty of the virtual machine host, go ahead and click accept.

### theme development

From the `site` directory be sure to install the deps first with: `npm i` and then run the following command while developing locally: `npm start`. The start command will run [webpack](https://webpack.js.org/) with the watch flag and recompile the theme's JS + SCSS files. Assets are also automatically compiled during the deployment process which can be configured here: `./trellis/deploy-hooks/build-before.yml`.

## Project Structure

As noted at the beginning of this readme, this project uses [Roots.io](https://roots.io/). [Trellis](https://roots.io/trellis/) for dev + prod server management and [Bedrock](https://roots.io/bedrock/) for dev tools, configuration and structure. Here are a few notable files and/or customizations:

### Trellis

- `./trellis/deploy-hooks/build-before.yml` is used to configure custom steps in the deployment process, like compiling the JS + SCSS.
- `./trellis/group_vars/` contains various settings for each environment including things like database connection info, SSL, cache settings, and PHP overrides.
- `./trellis/nginx-includes/coconutbliss.com/` contains a couple of nginx configurations like redirects and http cache response headers.

### Bedrock / Site

- `./site/config/environments/` contains WP settings for each env. For instance, `DISALLOW_FILE_MODS` is set to true which disables installing/updating plugins + the WP core. We do this 1) because we use composer to manage dependencies, but 2) to prevent users from accidentally breaking the production site because of a plugin compatability issue (instead the updates should be tested in staging or development first and their versions updated in the `composer.json`).
- `./site/composer.json` includes all the theme plugin dependencies and WP core version setting. A number of paid plugins are in use on the site which are stored in private Git repos, including (but not limited to): Gravity Forms, Slider Revolution, WPBakery / Visual Composer.
- `./site/web/app/themes/cb2019/` is the new active theme.
- `./site/web/app/themes/coconutbliss/` is the old theme (some code was copied over from that theme to create the new one, so we left it in here for reference).

## Deployment

Before you can deploy anything though, you will need the vault password and to have another developer add your SSH key to the server's authorized keys.

First create a file at `./trellis/.vault_pass` and get the password (to paste into it) from another dev. Then make sure to have them add your SSH key to `/home/web/.ssh/authorized_keys`.

Then with that, run the following from the `trellis` directory to deploy to staging:

```
./bin/deploy.sh staging florabowley.com
```

or for production:

```
./bin/deploy.sh production florabowley.com
```

## Troubleshooting

- Permission Denied (publickey)

If you run into issues with the deploy step: "deploy : Clone Project Files" and/or "deploy : Failed connection to remote repo" or during development related to the step: "wordpress-install : Install Dependencies with Composer" and you see "Permission Denied (publickey)" you may need to make sure your SSH key is loaded into your local ssh agent. Check that it is by running:

```
ssh-add -l
```

You should see `/Users/YOUR_USER_NAME/.ssh/id_rsa` (or whatever SSH key you have connected to github) in the output. If you don't see this, run:

```
ssh-add -K ~/.ssh/id_rsa
```

- mount.nfs: access denied by server while mounting [ip_address]:[/path/to/site]

If you are running software that uses the NFS mounting tools, like "Local by FlyWheel", you may run into issues with overlapping folders being shared. View/Edit `/etc/exports` and make sure the rules don't conflict with each other.

## Build Process

/trellis/deploy-hooks/build-before.yml

## SSH + Remote File Locations

To connect to the server run:

```
ssh ubuntu@REMOTE_HOST_URL
```

The site files are located at: `/srv/www/florabowley.com`
